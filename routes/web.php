<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',
	function () {
	    return redirect('/login');
	}
);
Route::post('/authentication', 'LoginController@authentication');
Route::get('/logout', 'LoginController@logout');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');


Route::get('/associate/profile', 'Admin\AssociateController@profile');
Route::get('/investor/profile', 'Admin\InvestorController@profile');
Route::get('/tender/profile', 'Admin\TenderController@profile');
Route::get('/stall/assign/{id}', 'Admin\StallController@assignProducts');

Route::get('/associate/trash', 'Admin\AssociateController@trash');
Route::get('/investor/trash', 'Admin\InvestorController@trash');
Route::get('/tender/trash', 'Admin\TenderController@trash');
Route::get('/stall/trash', 'Admin\StallController@trash');
Route::get('/product/trash', 'Admin\ProductController@trash');


Route::group(['prefix' => 'admin', 'namespace' => 'Admin'],function(){
	Route::post('/associate/status', 'AssociateController@changeStatus');
	Route::resource('/associate', 'AssociateController');
	Route::post('/associate/destroy/{id}', 'AssociateController@destroy');
	
	
	Route::post('/investor/status', 'InvestorController@changeStatus');
	Route::resource('/investor', 'InvestorController');
	Route::post('/investor/destroy/{id}', 'InvestorController@destroy');
	
	Route::resource('/tender', 'TenderController');
	Route::post('/tender/status', 'TenderController@changeStatus');
	Route::post('/tender/destroy/{id}', 'TenderController@destroy');
	
	
	Route::resource('/category', 'CategoryController');
	Route::post('/category/status', 'CategoryController@changeStatus');
	Route::post('/category/destroy/{id}', 'CategoryController@destroy');

	Route::resource('/subcategory', 'SubcategoryController');
	Route::post('/subcategory/status', 'SubcategoryController@changeStatus');
	Route::post('/subcategory/getSubcategory', 'SubcategoryController@getSubcategory');
	Route::post('/subcategory/destroy/{id}', 'SubcategoryController@destroy');


	Route::resource('/profile', 'ProfileController');

	Route::post('/profile_update', 'ProfileController@profileUpdate');

	Route::resource('/stall', 'StallController');
	Route::post('/stall/status', 'StallController@changeStatus');
	Route::post('/stall/destroy/{id}', 'StallController@destroy');
	Route::post('/stall/saveproduct', 'StallController@saveproduct');
	Route::post('/stall/removeproduct', 'StallController@removeproduct');
	Route::post('/stall/changequantity', 'StallController@changequantity');
	Route::post('/stall/checkquantity', 'StallController@checkquantity');
	

	Route::resource('/qty_type', 'ProductQuantityType');
	Route::post('/qty_type/status', 'ProductQuantityType@changeStatus');
	Route::post('/qty_type/destroy/{id}', 'ProductQuantityType@destroy');

	Route::resource('/product', 'ProductController');
	Route::post('/product/status', 'ProductController@changeStatus');
	Route::post('/product/destroy/{id}', 'ProductController@destroy');
	

	Route::post('/investor/getTenderToAssign', 'InvestorController@getTendersForInvestor');
	Route::post('/assignTender', 'InvestorController@assignTender');

	Route::resource('/order', 'OrderController');

});