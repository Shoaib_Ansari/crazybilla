<div class="page-footer">
	<div class="page-footer-inner">
		{{ date('Y') }} &copy; Food Truck by BigCity.
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
