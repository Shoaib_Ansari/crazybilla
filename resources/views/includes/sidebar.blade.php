@php
	$menu 		= explode('-', $menu);
	$main_menu 	= $menu[0];
	$submenu 	= $menu[1];
@endphp
<div class="page-sidebar-wrapper">

	<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
	<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
	<div class="page-sidebar md-shadow-z-2-i  navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
		<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
		<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
		<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
			<li class="start 
				@if ($main_menu == 'dashboard')
					active
				@endif
			">
				<a href="{{url('home')}}">
				<i class="icon-home"></i>
				<span class="title">Dashboard</span>
				</a>
			</li>
			@if (Auth::user()->user_type == 'admin')
				<li
				@if ($main_menu == 'associate')
					class="active"
				@endif
				>
					<a href="{{url('admin/associate')}}">
						<i class="icon-user"></i>
						<span class="title">Associate Management</span>
					</a>
				</li>
			@endif
			@if (Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'associate')
			<li
				@if ($main_menu == 'investor')
					class="active"
				@endif
			>
				<a href="{{url('admin/investor')}}">
					<i class="icon-envelope-open"></i>
					<span class="title">Investor Management</span>
				</a>
			</li>
			@endif
			<li
				@if ($main_menu == 'category')
					class="active"
				@endif
			>
				<a href="{{url('admin/category')}}">
					<i class="fa fa-th-large"></i>
					<span class="title">Category Management</span>
				</a>
			</li>
			<li
				@if ($main_menu == 'subcategory')
					class="active"
				@endif
			>
				<a href="{{url('admin/subcategory')}}">
					<i class="fa fa-th"></i>
					<span class="title">Subcategory Management</span>
				</a>
			</li>
			<li
				@if ($main_menu == 'quantity')
					class="active"
				@endif
			>
				<a href="{{url('admin/qty_type')}}">
					<i class="fa fa-flask"></i>
					<span class="title">Product Quantity</span>
				</a>
			</li>
			<li
				@if ($main_menu == 'tender')
					class="active"
				@endif
			>
				<a href="{{url('admin/tender')}}">
					<i class="icon-briefcase"></i>
					<span class="title">Tenders</span>
				</a>
			</li>
			<li
				@if ($main_menu == 'stall')
					class="active"
				@endif
			>
				<a href="{{url('admin/stall')}}">
					<i class="icon-basket"></i>
					<span class="title">Stall</span>
				</a>
			</li>

			<li
				@if ($main_menu == 'product')
					class="active"
				@endif
			>
				<a href="{{url('admin/product')}}">
				<i class="fa fa-cutlery"></i>
				<!-- <i class="fa fa-archive"></i> -->
				<span class="title">Products</span>
				</a>
			</li>
			<li
				@if ($main_menu == 'order')
					class="active"
				@endif
			>
				<a href="{{url('admin/order')}}">
				<i class="fa fa-cutlery"></i>
				<span class="title">Orders</span>
				</a>
			</li>
			<li
				@if ($main_menu == 'trash')
					class="active"
				@endif
			>
				<a href="javascript:;">
				<i class="fa fa-trash"></i>
				<span class="title">Trash</span>
				<span class="arrow "></span>
				</a>
				<ul class="sub-menu">
					@if (Auth::user()->user_type == 'admin')
					<li
						@if ($submenu == 'associate')
							class="active"
						@endif
					>

						<a href="{{url('associate/trash')}}">
						Associate</a>
					</li>
					@endif
					@if (Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'associate')
					<li
						@if ($submenu == 'investor')
							class="active"
						@endif
					>
						<a href="{{url('investor/trash')}}">
						Investor</a>
					</li>
					@endif
					<li
						@if ($submenu == 'tender')
							class="active"
						@endif
					>
						<a href="{{url('tender/trash')}}">
						Tenders</a>
					</li>
					<li
						@if ($submenu == 'stall')
							class="active"
						@endif
					>
						<a href="{{url('stall/trash')}}">
						Stall</a>
					</li>
					<li
						@if ($submenu == 'product')
							class="active"
						@endif
					>
						<a href="{{url('product/trash')}}">
						Products</a>
					</li>
				</ul>
			</li>
		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
</div>