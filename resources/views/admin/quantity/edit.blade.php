<div class="page-content-wrapper">
	<div class="page-content">
		<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<form role="update-qty_type" action="{{ url('admin/qty_type/'.___encrypt($QtyDetails['id'])) }}" method="POST" class="horizontal-form">
				<div class="form-body">
					<input type="hidden" name="_method" value="PUT">
					<input type="hidden" name="action" value="edit">
					<h3 class="form-section">{{ $page_title }}<span class="pull-right" style="font-size: 12px;">All fields marked with <span style="color: red">*</span> are manadatory</span></h3>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Quantity Name<span style="color: red">*</span></label>
								<input type="text" value="{{ $QtyDetails['name'] }}" name="name" class="form-control" placeholder="Enter Category Name">
								
							</div>
						</div>
				</div>
				<div class="form-actions right">
					<a href="{{url('admin/qty_type')}}" class="btn default">Cancel</a>
					<button type="button" data-request="ajax-submit" data-target='[role="update-qty_type"]' class="btn blue"><i class="fa fa-check"></i> Save</button>
				</div>
			</form>
			<!-- END FORM-->
		</div>		
	</div>
</div>