<div class="page-content-wrapper">
	<div class="page-content">
		<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<form role="update-tender" action="{{url('admin/tender/'.___encrypt($tenderList['user_id']))}}" method="POST" class="horizontal-form">
				<div class="form-body">
					<input type="hidden" name="_method" value="PUT">
					<input type="hidden" name="action" value="edit">
					<h3 class="form-section">{{$page_title}}<span class="pull-right" style="font-size: 12px;">All fields marked with <span style="color: red">*</span> are manadatory</span></h3>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">Name<span style="color: red">*</span></label>
								<input type="text" value="{{$tenderList['name']}}" name="name" class="form-control" placeholder="Enter First Name">
								
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Email<span style="color: red">*</span></label>
								<input type="text" name="email" class="form-control" placeholder="Enter Email" value="{{ $tenderList['tender']['email'] }}">
							</div>
						</div>
						<!--/span-->
						    {{ csrf_field() }}

						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Availability<span style="color: red">*</span></label>
								<select class="form-control" name="availability">
									@foreach ($AvailabilityList as $availability) 
								        <option value="{{ $availability['id'] }}"
								        @if($availability['id'] == $tenderList['availability'])
								        selected="selected"
								        @endif 
								        >{{ $availability['name'] }}</option>
								    @endforeach
								</select>
							</div>
						</div>
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Aadhar No.<span style="color: red">*</span></label>
								<input type="text" name="aadhar_no" class="form-control" placeholder="Enter Aadhar Number" value="{{ $tenderList['tender']['aadhar_no'] }}" minlength="12" maxlength="12">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Pan Card No.<span style="color: red">*</span></label>
								<input type="text" name="pan_no" class="form-control" placeholder="Enter Pan Card Number" value="{{ $tenderList['tender']['pan_no'] }}" minlength="10" maxlength="10">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">Country Code</label>
										<select class="form-control" name="country_code">
											<option value="+91">+91</option>
										</select>
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group">
										<label class="control-label">Mobile Number<span style="color: red">*</span></label>
										<input type="text" name="mobile" class="form-control" placeholder="Enter Mobile Number" minlength="10" maxlength="10" value="{{ $tenderList['tender']['mobile'] }}">
									</div>
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">Country Code</label>
										<select class="form-control" name="country_code">
											<option value="+91">+91</option>
										</select>
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group">
										<label class="control-label">Alternate Mobile Number</label>
										<input type="text" name="alternate_phone" class="form-control" placeholder="Enter Mobile Number" minlength="10" maxlength="10" value="{{ $tenderList['alternate_phone'] }}">
									</div>
								</div>
							</div>
						</div>
						
						<!--/span-->
					</div>
					<div class="row">
						<div class="col-md-12 ">
							<div class="form-group">
								<label>Profile Picture</label>
								<input type="file" class="form-control" name="profile_picture">
								<input type="hidden" name="old_profile_picture" value="{{ $tenderList['tender']['profile_picture'] }}">
							</div>
						</div>
					</div>
					<h3 class="form-section">Address</h3>
					<div class="row">
						<div class="col-md-12 ">
							<div class="form-group">
								<label>Street<span style="color: red">*</span></label>
								<input type="text" class="form-control" placeholder="Enter Street" name="street" value="{{ $tenderList['adresses'][0]['street'] }}">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>City<span style="color: red">*</span></label>
								<input type="text" class="form-control" placeholder="Enter City" name="city" value="{{ $tenderList['adresses'][0]['city'] }}">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label>State<span style="color: red">*</span></label>
								<input type="text" class="form-control" placeholder="Enter State" name="state" value="{{ $tenderList['adresses'][0]['state'] }}">
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Post Code<span style="color: red">*</span></label>
								<input type="text" class="form-control" placeholder="Enter Post Code" name="pin_code" value="{{ $tenderList['adresses'][0]['pin_code'] }}">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label>Country<span style="color: red">*</span></label>
								<select class="form-control" name="country">
									<option value="">select Country</option>
									<option value="India"
									@if ($tenderList['adresses'][0]['country'] == "India")
										selected="selected"
									@endif
									>India</option>
									<option value="Singapore"
									@if ($tenderList['adresses'][0]['country'] == "Singapore")
										selected="selected"
									@endif
									>Singapore</option>
								</select>
							</div>
						</div>
						<!--/span-->
					</div>
					<h3 class="form-section">Set a password<span style="color: red">*</span></h3>
					<p>Note: If you dont want to change the password then left the below fields blank</p>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Password</label>
								<input type="password" class="form-control" placeholder="Enter Password" name="password">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label>Confirm Password<span style="color: red">*</span></label>
								<input type="password" class="form-control" placeholder="Enter Confirm Password" name="confirm_password">
							</div>
						</div>
						<!--/span-->
					</div>
				</div>
				<div class="form-actions right">
					<a href="{{url('admin/associate')}}" class="btn default">Cancel</a>
					<button type="button" data-request="ajax-submit" data-target='[role="update-tender"]' class="btn blue"><i class="fa fa-check"></i> Save</button>
				</div>
			</form>
			<!-- END FORM-->
		</div>		
	</div>
</div>