<div class="page-content-wrapper">
	<div class="page-content">
		<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<form role="update-associate" action="{{url('admin/associate/'.___encrypt($associateDetails['user_id']))}}" method="POST" class="horizontal-form">
				<div class="form-body">
					<input type="hidden" name="_method" value="PUT">
					<input type="hidden" name="action" value="edit">
					<h3 class="form-section">{{$page_title}}<span class="pull-right" style="font-size: 12px;">All fields marked with <span style="color: red">*</span> are manadatory</span></h3>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">First Name<span style="color: red">*</span></label>
								<input type="text" id="firstName" value="{{$associateDetails['first_name']}}" name="first_name" class="form-control" placeholder="Enter First Name">
								
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Last Name<span style="color: red">*</span></label>
								<input type="text" id="lastName" value="{{$associateDetails['last_name']}}" name="last_name" class="form-control" placeholder="Enter Last Name">
								
							</div>
						</div>
						<!--/span-->
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Email<span style="color: red">*</span></label>
								<input type="text" id="firstName" value="{{$associateDetails['email']}}" name="email" class="form-control" placeholder="Enter Email">
								
							</div>
						</div>
						<!--/span-->
						    {{ csrf_field() }}

						<div class="col-md-6">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">Country Code</label>
										<select class="form-control" name="country_code">
											<option value="+91">+91</option>
										</select>
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group">
										<label class="control-label">Mobile Number<span style="color: red">*</span></label>
										<input type="text" id="lastName" value="{{$associateDetails['mobile_number']}}" name="mobile" class="form-control" placeholder="Enter Mobile Number">
										
									</div>
								</div>
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Gender</label>
								<select class="form-control" name="gender">
									<option value="male" @if(strtolower($associateDetails['gender'])=="male") selected="selected" 
									@endif>Male</option>
									<option value="female" @if(strtolower($associateDetails['gender'])=="female") selected="selected" 
									@endif>Female</option>
								</select>
								<!-- <span class="help-block">
								Select your gender </span> -->
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Date of Birth<span style="color: red">*</span></label>
								<input type="date" class="form-control" value="{{$associateDetails['dob']}}" name="date_of_birth" placeholder="mm/dd/yyyy">
							</div>
						</div>
						<!--/span-->
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Aadhaar Number<span style="color: red">*</span></label>
								<input type="text" class="form-control" value="{{$associateDetails['associate']['aadhar_no']}}" name="aadhar_no" placeholder="Enter Adahaar Number">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">PAN Number<span style="color: red">*</span></label>
								<input type="text" class="form-control" value="{{$associateDetails['associate']['pan_no']}}" name="pan_no" placeholder="Enter PAN Number">
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<!-- <div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Category</label>
								<select class="select2_category form-control" name="Category" data-placeholder="Choose a Category" tabindex="1">
									<option value="Category 1">Category 1</option>
									<option value="Category 2">Category 2</option>
									<option value="Category 3">Category 5</option>
									<option value="Category 4">Category 4</option>
								</select>
							</div>
						</div>
					
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Membership</label>
								<div class="radio-list">
									<label class="radio-inline">
									<input type="radio" name="membership" id="membership1" value="option1" checked> Option 1 </label>
									<label class="radio-inline">
									<input type="radio" name="membership" id="membership2" value="option2"> Option 2 </label>
								</div>
							</div>
						</div>
					</div> -->
					<!--/row-->
					<div class="row">
						<div class="col-md-12 ">
							<div class="form-group">
								<label>Profile Picture</label>
								<input type="file" class="form-control" name="profile_picture">
								<input type="hidden" name="old_profile_picture" value="{{$associateDetails['associate']['profile_picture']}}">
							</div>
						</div>
					</div>
					<h3 class="form-section">Address</h3>
					<div class="row">
						<div class="col-md-12 ">
							<div class="form-group">
								<label>Street<span style="color: red">*</span></label>
								<input type="text" class="form-control" value="{{$associateDetails['street']}}" placeholder="Enter Street" name="street">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>City<span style="color: red">*</span></label>
								<input type="text" class="form-control" value="{{$associateDetails['city']}}" placeholder="Enter City" name="city">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label>State<span style="color: red">*</span></label>
								<input type="text" class="form-control" value="{{$associateDetails['state']}}" placeholder="Enter State" name="state">
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Post Code<span style="color: red">*</span></label>
								<input type="text" class="form-control" value="{{$associateDetails['post_code']}}" placeholder="Enter Post Code" name="pin_code">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label>Country<span style="color: red">*</span></label>
								<select class="form-control" name="country">
									<option value="">Select Country</option>
									<option value="india" @if(strtolower($associateDetails['country'])=="india") selected="selected" 
									@endif>India</option>
									<option value="singapore" @if(strtolower($associateDetails['country'])=="singapore") selected="selected" @endif>Singapore</option>
								</select>
							</div>
						</div>
						<!--/span-->
					</div>
					<!-- <div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Password</label>
								<input type="password" class="form-control" placeholder="Enter Password" name="password">
							</div>
						</div>
						
						<div class="col-md-6">
							<div class="form-group">
								<label>Confirm Password</label>
								<input type="password" class="form-control" placeholder="Enter Confirm Password" name="confirm_password">
							</div>
						</div>
						
					</div> -->
				</div>
				<div class="form-actions right">
					<a href="{{url('admin/associate')}}" class="btn default">Cancel</a>
					<button type="button" data-request="ajax-submit" data-target='[role="update-associate"]' class="btn blue"><i class="fa fa-check"></i> Save</button>
				</div>
			</form>
			<!-- END FORM-->
		</div>		
	</div>
</div>