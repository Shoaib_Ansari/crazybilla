


<div class="page-content-wrapper">
	<div class="page-content">
		<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<form role="add-associate" action="{{url('admin/associate')}}" method="POST" class="horizontal-form">
				<div class="form-body">
					<h3 class="form-section">Add Associate<span class="pull-right" style="font-size: 12px;">All fields marked with <span style="color: red">*</span> are manadatory</span></h3>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">First Name<span style="color: red">*</span></label>
								<input type="text" id="firstName" name="first_name" class="form-control" placeholder="Enter First Name">
								
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Last Name<span style="color: red">*</span></label>
								<input type="text" id="lastName" name="last_name" class="form-control" placeholder="Enter Last Name">
								
							</div>
						</div>
						<!--/span-->
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Email<span style="color: red">*</span></label>
								<input type="text" id="firstName" name="email" class="form-control" placeholder="Enter Email">
								
							</div>
						</div>
						<!--/span-->
						    {{ csrf_field() }}

						<div class="col-md-6">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">Country Code</label>
										<select class="form-control" name="country_code">
											<option value="+91">+91</option>
										</select>
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group">
										<label class="control-label">Mobile Number<span style="color: red">*</span></label>
										<input type="text" id="lastName" name="mobile" class="form-control" placeholder="Enter Mobile Number" maxlength="10" maxlength="10">
										
									</div>
								</div>
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Gender</label>
								<select class="form-control" name="gender">
									<option value="male">Male</option>
									<option value="female">Female</option>
								</select>
								<span class="help-block">
								Select your gender </span>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Date of Birth<span style="color: red">*</span></label>
								<input type="date" class="form-control" name="date_of_birth" placeholder="mm/dd/yyyy">
							</div>
						</div>
						<!--/span-->
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Aadhaar Number<span style="color: red">*</span></label>
								<input type="text" class="form-control" name="aadhar_no" placeholder="Enter Adahaar Number" maxlength="12" minlength="12">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">PAN Number<span style="color: red">*</span></label>
								<input type="text" class="form-control" name="pan_no" placeholder="Enter PAN Number" maxlength="10" maxlength="10">
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<!-- <div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Category</label>
								<select class="select2_category form-control" name="Category" data-placeholder="Choose a Category" tabindex="1">
									<option value="Category 1">Category 1</option>
									<option value="Category 2">Category 2</option>
									<option value="Category 3">Category 5</option>
									<option value="Category 4">Category 4</option>
								</select>
							</div>
						</div>
					
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Membership</label>
								<div class="radio-list">
									<label class="radio-inline">
									<input type="radio" name="membership" id="membership1" value="option1" checked> Option 1 </label>
									<label class="radio-inline">
									<input type="radio" name="membership" id="membership2" value="option2"> Option 2 </label>
								</div>
							</div>
						</div>
					</div> -->
					<!--/row-->
					<div class="row">
						<div class="col-md-12 ">
							<div class="form-group">
								<label>Profile Picture</label>
								<input type="file" class="form-control" name="profile_picture">
								
							</div>
						</div>
					</div>
					<!-- <div class="form-group">
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
								<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
							</div>
							<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
							</div>
							<div>
								<span class="btn default btn-file">
								<span class="fileinput-new">
								Select image </span>
								<span class="fileinput-exists">
								Change </span>
								<input type="file" name="profile_picture">
								</span>
								<a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
								Remove </a>
							</div>
						</div>
					</div> -->
					
					<h3 class="form-section">Address</h3>
					<div class="row">
						<div class="col-md-12 ">
							<div class="form-group">
								<label>Street<span style="color: red">*</span></label>
								<input type="text" class="form-control" placeholder="Enter Street" name="street">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>City<span style="color: red">*</span></label>
								<input type="text" class="form-control" placeholder="Enter City" name="city">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label>State<span style="color: red">*</span></label>
								<input type="text" class="form-control" placeholder="Enter State" name="state">
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Post Code<span style="color: red">*</span></label>
								<input type="text" class="form-control" placeholder="Enter Post Code" name="pin_code" minlength="4" maxlength="6">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label>Country<span style="color: red">*</span></label>
								<select class="form-control" name="country">
									<option value="">Select Country</option>
									<option value="India">India</option>
									<option value="Singapore">Singapore</option>
								</select>
							</div>
						</div>
						<!--/span-->
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Password<span style="color: red">*</span></label>
								<input type="password" class="form-control" placeholder="Enter Password" name="password">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label>Confirm Password<span style="color: red">*</span></label>
								<input type="password" class="form-control" placeholder="Enter Confirm Password" name="confirm_password">
							</div>
						</div>
						<!--/span-->
					</div>
				</div>
				<div class="form-actions right">
					<a href="{{url('admin/associate')}}" class="btn default">Cancel</a>
					<button type="button" data-request="ajax-submit" data-target='[role="add-associate"]' class="btn blue"><i class="fa fa-check"></i> Save</button>
				</div>
			</form>
			<!-- END FORM-->
		</div>		
	</div>
</div>


