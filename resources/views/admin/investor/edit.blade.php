<div class="page-content-wrapper">
	<div class="page-content">
		<div class="portlet-body form">

			<!-- BEGIN FORM-->
			<form role="edit-investor" action="{{url('admin/investor/'.___encrypt($investorDetails['user_id']))}}" method="POST" class="horizontal-form">
				<input type="hidden" name="_method" value="PUT">
				<input type="hidden" name="action" value="edit">
				<div class="form-body">
					<h3 class="form-section">Edit Investor<span class="pull-right" style="font-size: 12px;">All fields marked with <span style="color: red">*</span> are manadatory</span></h3>
					@if (Auth::user()->user_type == 'admin')
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">Associate<span style="color: red">*</span></label>
								<select class="form-control" name="associate_id">
									<option value="">Select associate</option>
									@foreach ($associateList as $associate) 
								        <option value="{{ $associate['id'] }}" 
								        @if ($associate['id'] == $investorDetails['associate_id'])
								        	selected="selected"
								        @endif 
								        >{{ $associate['name'] }}</option>
								    @endforeach
								</select>
							</div>
						</div>
					</div>
					@else
					<input type="hidden" name="associate_id" value="{{ Auth::user()->id }}">
					@endif
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Name<span style="color: red">*</span></label>
								<input type="text" name="name" class="form-control" placeholder="Enter First Name" value="{{ $investorDetails['name'] }}">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Father's Name</label>
								<input type="text" name="father_name" class="form-control" placeholder="Enter father's Name" value="{{ $investorDetails['father_name'] }}">
							</div>
						</div>
						<!--/span-->
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Email<span style="color: red">*</span></label>
								<input type="text" name="email" class="form-control" placeholder="Enter Email"  value="{{ $investorDetails['email'] }}">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Gender</label>
								<select class="form-control" name="gender">
									<option value="male"
									@if ($investorDetails['gender'] == 'male')
									selected="selected"
									@endif 
									>Male</option>
									<option value="female"
									@if ($investorDetails['gender'] == 'female')
									selected="selected"
									@endif
									>Female</option>
								</select>
							</div>
						</div>
						
						<!--/span-->
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Aadhar No.<span style="color: red">*</span></label>
								<input type="text" name="aadhar_no" class="form-control" placeholder="Enter Aadhar Number" value="{{ $investorDetails['investor']['aadhar_no'] }}" minlength="12" maxlength="12">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Pan Card No.<span style="color: red">*</span></label>
								<input type="text" name="pan_no" class="form-control" placeholder="Enter Pan Card Number" value="{{ $investorDetails['investor']['pan_no'] }}" minlength="10" maxlength="10">
							</div>
						</div>
						
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">Country Code</label>
										<select class="form-control" name="country_code">
											<option value="+91">+91</option>
										</select>
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group">
										<label class="control-label">Mobile Number<span style="color: red">*</span></label>
										<input type="text" id="lastName" name="phone" class="form-control" placeholder="Enter Mobile Number" value="{{ $investorDetails['phone'] }}" minlength="10" maxlength="10">
									</div>
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">Country Code</label>
										<select class="form-control" name="alternate_country_code">
											<option value="+91">+91</option>
										</select>
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group">
										<label class="control-label">Alternate Mobile Number</label>
										<input type="text" id="lastName" name="alternate_phone" class="form-control" placeholder="Enter Mobile Number" value="{{ $investorDetails['alternate_phone'] }}" minlength="10" maxlength="10">
									</div>
								</div>
							</div>
						</div>
						
						<!--/span-->
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Photo<span style="color: red">*</span></label>
								<input type="file" class="form-control" name="profile_picture">
								<input type="hidden" name="old_profile_picture" value="{{$investorDetails['investor']['profile_picture']}}">
							</div>
						</div>
					</div>
					<h3 class="form-section">Investment</h3>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">Amount</label>
								<input type="number" name="invested_amount" class="form-control" style="" value="{{ $investorDetails['invested_amount'] }}" readonly="">
							</div>
						</div>
					</div>
					<h3 class="form-section">Address</h3>
					<div class="row">
						<div class="col-md-12 ">
							<div class="form-group">
								<label>Street<span style="color: red">*</span></label>
								<input type="text" class="form-control" placeholder="Enter Street" name="street" value="{{ $investorDetails['adresses'][0]['street'] }}">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>City<span style="color: red">*</span></label>
								<input type="text" class="form-control" placeholder="Enter City" name="city" value="{{ $investorDetails['adresses'][0]['city'] }}">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label>State<span style="color: red">*</span></label>
								<input type="text" class="form-control" placeholder="Enter State" name="state" value="{{ $investorDetails['adresses'][0]['state'] }}">
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Post Code<span style="color: red">*</span></label>
								<input type="text" class="form-control" placeholder="Enter Post Code" name="pin_code" value="{{ $investorDetails['adresses'][0]['pin_code'] }}" minlength="4" maxlength="6">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label>Country<span style="color: red">*</span></label>
								<select class="form-control" name="country">
									<option value="">Select Country</option>
									<option value="India"
									@if ($investorDetails['adresses'][0]['country'] == "India")
										selected="selected"
									@endif 
									>India</option>
									<option value="Singapore"
									@if ($investorDetails['adresses'][0]['country'] == "Singapore")
										selected="selected"
									@endif
									>Singapore</option>
								</select>
							</div>
						</div>
						<!--/span-->
					</div>
					<h3 class="form-section">Set a password</h3>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Password</label>
								<input type="password" class="form-control" placeholder="Enter Password" name="password">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Confirm Password</label>
								<input type="password" class="form-control" placeholder="Enter Confirm Password" name="confirm_password">
							</div>
						</div>
					</div>
				</div>
				<div class="form-actions right">
					<a href="{{url('admin/investor')}}" class="btn default">Cancel</a>
					<button type="button" data-request="ajax-submit" data-target='[role="edit-investor"]' class="btn blue"><i class="fa fa-check"></i> Update</button>
				</div>
			</form>
			<!-- END FORM-->
		</div>		
	</div>
</div>