<div class="page-content-wrapper">
	<div class="page-content">
		<div class="portlet-body form">

			<!-- BEGIN FORM-->
			<form role="add-investor" action="{{url('admin/investor')}}" method="POST" class="horizontal-form">
				<div class="form-body">
					<h3 class="form-section">Add Investor<span class="pull-right" style="font-size: 12px;">All fields marked with <span style="color: red">*</span> are manadatory</span></h3>
					@if (Auth::user()->user_type == 'admin')
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">Associate<span style="color: red">*</span></label>
								<select class="form-control" name="associate_id">
									<option value="">Select associate</option>
									@foreach ($associateList as $associate) 
								        <option value="{{ $associate['id'] }}">{{ $associate['name'] }}</option>
								    @endforeach
								</select>
							</div>
						</div>
					</div>
					@else
					<input type="hidden" name="associate_id" value="{{ Auth::user()->id }}">
					@endif
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Name<span style="color: red">*</span></label>
								<input type="text" name="name" class="form-control" placeholder="Enter First Name">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Father's Name</label>
								<input type="text" name="father_name" class="form-control" placeholder="Enter father's Name">
							</div>
						</div>
						<!--/span-->
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Email<span style="color: red">*</span></label>
								<input type="text" name="email" class="form-control" placeholder="Enter Email">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Gender</label>
								<select class="form-control" name="gender">
									<option value="male">Male</option>
									<option value="female">Female</option>
								</select>
							</div>
						</div>
						
						<!--/span-->
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Aadhar No.<span style="color: red">*</span></label>
								<input type="text" name="aadhar_no" class="form-control" placeholder="Enter Aadhar Number" minlength="12" maxlength="12">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Pan Card No.<span style="color: red">*</span></label>
								<input type="text" name="pan_no" class="form-control" placeholder="Enter Pan Card Number" minlength="10" maxlength="10">
							</div>
						</div>
						
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">Country Code</label>
										<select class="form-control" name="country_code">
											<option value="+91">+91</option>
										</select>
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group">
										<label class="control-label">Mobile Number<span style="color: red">*</span></label>
										<input type="text" id="lastName" name="phone" class="form-control" placeholder="Enter Mobile Number" minlength="10" maxlength="10">
									</div>
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">Country Code</label>
										<select class="form-control" name="country_code">
											<option value="+91">+91</option>
										</select>
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group">
										<label class="control-label">Alternate Mobile Number</label>
										<input type="text" id="lastName" name="alternate_phone" class="form-control" placeholder="Enter Mobile Number" minlength="10" maxlength="10">
									</div>
								</div>
							</div>
						</div>
						
						<!--/span-->
					</div>
					<div class="row">
						<!-- <div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Date of Birth*</label>
								<input type="date" class="form-control" name="date_of_birth" placeholder="mm/dd/yyyy">
							</div>
						</div> -->
						<div class="col-md-12">
							<div class="form-group">
								<label>Photo<span style="color: red">*</span></label>
								<input type="file" class="form-control" name="profile_picture">
							</div>
						</div>
					</div>
					<h3 class="form-section">Investment</h3>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">Amount</label>
								<input type="number" name="invested_amount" class="form-control" style="" value="60000" readonly="">
							</div>
						</div>
					</div>
					<h3 class="form-section">Address</h3>
					<div class="row">
						<div class="col-md-12 ">
							<div class="form-group">
								<label>Street<span style="color: red">*</span></label>
								<input type="text" class="form-control" placeholder="Enter Street" name="street">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>City<span style="color: red">*</span></label>
								<input type="text" class="form-control" placeholder="Enter City" name="city">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label>State<span style="color: red">*</span></label>
								<input type="text" class="form-control" placeholder="Enter State" name="state">
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Post Code<span style="color: red">*</span></label>
								<input type="text" class="form-control" placeholder="Enter Post Code" name="pin_code" minlength="4" maxlength="6">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label>Country</label>
								<select class="form-control" name="country">
									<option value="India">India</option>
									<option value="Singapore">Singapore</option>
								</select>
							</div>
						</div>
						<!--/span-->
					</div>
					<h3 class="form-section">Set a password</h3>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Password</label>
								<input type="password" class="form-control" placeholder="Enter Password" name="password">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label>Confirm Password</label>
								<input type="password" class="form-control" placeholder="Enter Confirm Password" name="confirm_password">
							</div>
						</div>
						<!--/span-->
					</div>
				</div>
				<div class="form-actions right">
					<a href="{{url('admin/investor')}}" class="btn default">Cancel</a>
					<button type="button" data-request="ajax-submit" data-target='[role="add-investor"]' class="btn blue"><i class="fa fa-check"></i> Save</button>
				</div>
			</form>
			<!-- END FORM-->
		</div>		
	</div>
</div>