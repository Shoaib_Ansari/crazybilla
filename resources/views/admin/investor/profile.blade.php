
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1>User Account</h1>
				</div>
				<!-- END PAGE TITLE -->
				<!-- BEGIN PAGE TOOLBAR -->
				<div class="page-toolbar">
					<!-- BEGIN THEME PANEL -->
					<div class="btn-group btn-theme-panel">
						<a href="javascript:;" class="btn dropdown-toggle" data-toggle="dropdown">
						<i class="icon-settings"></i>
						</a>
						<div class="dropdown-menu theme-panel pull-right dropdown-custom hold-on-click">
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-12">
									<h3>THEME</h3>
									<ul class="theme-colors">
										<li class="theme-color theme-color-default active" data-theme="default">
											<span class="theme-color-view"></span>
											<span class="theme-color-name">Dark Header</span>
										</li>
										<li class="theme-color theme-color-light" data-theme="light">
											<span class="theme-color-view"></span>
											<span class="theme-color-name">Light Header</span>
										</li>
									</ul>
								</div>
								<div class="col-md-8 col-sm-8 col-xs-12 seperator">
									<h3>LAYOUT</h3>
									<ul class="theme-settings">
										<li>
											 Theme Style
											<select class="layout-style-option form-control input-small input-sm">
												<option value="square" selected="selected">Square corners</option>
												<option value="rounded">Rounded corners</option>
											</select>
										</li>
										<li>
											 Layout
											<select class="layout-option form-control input-small input-sm">
												<option value="fluid" selected="selected">Fluid</option>
												<option value="boxed">Boxed</option>
											</select>
										</li>
										<li>
											 Header
											<select class="page-header-option form-control input-small input-sm">
												<option value="fixed" selected="selected">Fixed</option>
												<option value="default">Default</option>
											</select>
										</li>
										<li>
											 Top Dropdowns
											<select class="page-header-top-dropdown-style-option form-control input-small input-sm">
												<option value="light">Light</option>
												<option value="dark" selected="selected">Dark</option>
											</select>
										</li>
										<li>
											 Sidebar Mode
											<select class="sidebar-option form-control input-small input-sm">
												<option value="fixed">Fixed</option>
												<option value="default" selected="selected">Default</option>
											</select>
										</li>
										<li>
											 Sidebar Menu
											<select class="sidebar-menu-option form-control input-small input-sm">
												<option value="accordion" selected="selected">Accordion</option>
												<option value="hover">Hover</option>
											</select>
										</li>
										<li>
											 Sidebar Position
											<select class="sidebar-pos-option form-control input-small input-sm">
												<option value="left" selected="selected">Left</option>
												<option value="right">Right</option>
											</select>
										</li>
										<li>
											 Footer
											<select class="page-footer-option form-control input-small input-sm">
												<option value="fixed">Fixed</option>
												<option value="default" selected="selected">Default</option>
											</select>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- END THEME PANEL -->
				</div>
				<!-- END PAGE TOOLBAR -->
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="index.html">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="#">Pages</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="#">User Account</a>
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PROFILE SIDEBAR -->
					<div class="profile-sidebar" style="width:250px;">
						<!-- PORTLET MAIN -->
						<div class="portlet light profile-sidebar-portlet">
							<!-- SIDEBAR USERPIC -->
							@php

							@endphp
							<div class="profile-userpic">
								<img src="{{ asset('images/'.Auth::user()->user_type.'/'.$UserDetails[0]['profile_picture']) }}" class="img-responsive" alt="">
							</div>
							<!-- END SIDEBAR USERPIC -->
							<!-- SIDEBAR USER TITLE -->
							<div class="profile-usertitle">
								<div class="profile-usertitle-name">
									 {{ Auth::user()->full_name }}
								</div>
							</div>
							
						</div>
						<!-- END PORTLET MAIN -->
						
					</div>
					<!-- END BEGIN PROFILE SIDEBAR -->
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
								<div class="portlet light">
									<div class="portlet-title tabbable-line">
										<div class="caption caption-md">
											<i class="icon-globe theme-font hide"></i>
											<span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
										</div>
										<ul class="nav nav-tabs">
											<li class="active">
												<a href="#tab_1_1" data-toggle="tab">Personal Info</a>
											</li>
											<li>
												<a href="#tab_1_3" data-toggle="tab">Change Password</a>
											</li>
											
										</ul>
									</div>
									<div class="portlet-body">
										<div class="tab-content">
											<!-- PERSONAL INFO TAB -->
										<div class="tab-pane active" id="tab_1_1">
											<form role="update-investor" action="{{url('admin/investor/'.___encrypt($profileId))}}" method="POST">
													<div class="col-md-12">
														<div class="col-md-6">
															<div class="form-group">
																<input type="hidden" name="_method" value="PUT">
																<input type="hidden" name="action" value="edit">
																<input type="hidden" name="updateProfile" value="updateProfile">
																<label class="control-label">Full Name<span style="color: red">*</span></label>
																<input type="text" placeholder="Enter Full Name" class="form-control" name="name" value="{{ $profileDetails[0]['name'] }}" />
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="control-label">Father's Name<span style="color: red">*</span></label>
																<input type="text" placeholder="Enter Father's Name" class="form-control" name="father_name" value="{{ $profileDetails[0]['father_name'] }}"/>
															</div>
														</div>
													</div>
													<div class="col-md-12">
														<div class="col-md-6">
															<div class="form-group">
																<label class="control-label">Mobile Number<span style="color: red">*</span></label>
																<input type="text" placeholder="Enter Mobile Number" class="form-control" 
																name="phone" value="{{ $profileDetails[0]['phone'] }}" />
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="control-label">Alternate Number</label>
																<input type="text" placeholder="Enter Alternate Number" class="form-control" 
																name="alternate_phone" value="{{ $profileDetails[0]['alternate_phone'] }}" />
															</div>
														</div>
													</div>
													<div class="col-md-12">
														<div class="col-md-6">
															<div class="form-group">
																<label class="control-label">Gender<span style="color: red">*</span></label>
																<select class="form-control" name="gender">
																	<option value="male" @if(strtolower($profileDetails[0]['gender'])=="male") selected="selected" 
																	@endif>Male</option>
																	<option value="female" @if(strtolower($profileDetails[0]['gender'])=="female") selected="selected" 
																	@endif>Female</option>
																</select>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="control-label">Street<span style="color: red">*</span></label>
																<input type="text" placeholder="Enter Street" class="form-control" name="street"
																value="{{ $addressDetails[0]['street'] }}"/>
															</div>
														</div>
													</div>
													<div class="col-md-12">
														<div class="col-md-6">
															<div class="form-group">
																<label class="control-label">City<span style="color: red">*</span></label>
																<input type="text" placeholder="Enter City" class="form-control" name="city" value="{{ $addressDetails[0]['city'] }}"/>
															</div>
														</div> 
														<div class="col-md-6">
															<div class="form-group">
																<label class="control-label">State<span style="color: red">*</span></label>
																<input type="text" placeholder="Enter State" class="form-control" name="state"
																value="{{ $addressDetails[0]['state'] }}"/>
															</div>
														</div>
													</div>
													<div class="col-md-12">
														<div class="col-md-6">
															<div class="form-group">
																<label class="control-label">Post Code<span style="color: red">*</span></label>
																<input type="text" placeholder="Enter Post Code" class="form-control" name="pin_code" value="{{ $addressDetails[0]['pin_code'] }}"/>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="control-label">Country<span style="color: red">*</span></label>
																<select class="form-control" name="country">
																  <option value="">Select Country</option>
																	<option value="india" @if(strtolower($addressDetails[0]['country'])=="india") selected="selected" 
																	@endif>India</option>
																	<option value="singapore" @if(strtolower($addressDetails[0]['country'])=="singapore") selected="selected" @endif>Singapore</option>
																</select>
															</div>
														</div>
													</div>
														<div class="col-md-12">
															<div class="form-group">
																<label>Photo</label>
																<input type="file" class="form-control" name="profile_picture">
																<input type="hidden" name="old_profile_picture" value="{{ Auth::user()->profile_picture }}">
															</div>
														</div>

													<div class="margin-top-10">
														<!-- <a href="" class="btn green-haze">Change Password </a> -->
														<button style="margin-left: 2%;" type="button" data-request="ajax-submit" data-target='[role="update-investor"]' class="btn blue"><i class=""></i> Save Changes
														</button>
														<a href="{{url('investor/profile')}}" class="btn default">
														Cancel </a> 
													</div>
												</form>
											</div>
											<!-- END PERSONAL INFO TAB -->
											
											<!-- CHANGE PASSWORD TAB -->
											<div class="tab-pane" id="tab_1_3">
												<form role="update-password" action="{{url('admin/profile/'.___encrypt($profileId))}}" method="POST">
													<div class="form-group">
														<input type="hidden" name="_method" value="PUT">
														<input type="hidden" name="action" value="edit">
														<label class="control-label">Current Password<span style="color: red">*</span></label>
														<input type="password" class="form-control" name="currentpassword" />
													</div>
													<div class="form-group">
														<label class="control-label">New Password<span style="color: red">*</span></label>
														<input type="password" class="form-control" name="password" />
													</div>
													<div class="form-group">
														<label class="control-label">Re-type New Password<span style="color: red">*</span></label>
														<input type="password" class="form-control" name="confirmpassword" />
													</div>
													<div class="margin-top-10">
														<!-- <a href="" class="btn green-haze">Change Password </a> -->
														<button type="button" data-request="ajax-submit" data-target='[role="update-password"]' class="btn blue"><i class=""></i> Change Password
														</button>
														<a href="{{url('investor/profile')}}" class="btn default">
														Cancel </a>
													</div>
												</form>
											</div>
											<!-- END CHANGE PASSWORD TAB -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
