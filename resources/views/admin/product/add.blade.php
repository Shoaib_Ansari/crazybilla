


<div class="page-content-wrapper">
	<div class="page-content">
		<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<form role="add-product" action="{{url('admin/product')}}" method="POST" class="horizontal-form">
				<div class="form-body">
					<h3 class="form-section">Add Product<span class="pull-right" style="font-size: 12px;">All fields marked with <span style="color: red">*</span> are manadatory</span></h3>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Category<span style="color: red">*</span></label>
								<select class="form-control" name="category_id" id="category_id">
									<option value="">Select Category</option>
									@foreach ($categoryList as $category) 
								        <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
								    @endforeach
								</select>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Subcategory</label>
								<select class="form-control" name="sub_category_id" id="sub_category_id">
									<option value="">Select Subcategory</option>
								</select>
							</div>
						</div>
						<!--/span-->
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Product Name<span style="color: red">*</span></label>
								<input type="text" id="productname" name="productname" class="form-control" placeholder="Enter Product Name">
								
							</div>
						</div>
						<!--/span-->
						    {{ csrf_field() }}

						<div class="col-md-6">
							<div class="form-group">
								<label>Product Picture<span style="color: red">*</span></label>
								<input type="file" class="form-control" name="image">
							</div>
						</div>
						
						<!--/span-->
					</div>
					
					
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label">Description<span style="color: red">*</span></label>
										<textarea rows="4" cols="50" id="productname" name="description" class="form-control" placeholder="Enter Description"></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Price<span style="color: red">*</span></label>
								<input type="text" class="form-control" placeholder="Enter Price" name="price">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label>Discount Percent<span style="color: red">*</span></label>
								<input type="text" class="form-control" placeholder="Enter Discount Percent" name="discount_percent">
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Discounted Price<span style="color: red">*</span></label>
								<input type="text" class="form-control" placeholder="Enter Discount Price" name="discount_price">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label>Quantity Type<span style="color: red">*</span></label>
								<select class="form-control" name="quantity_type">
									<option value="">Select Quantity Type</option>
									@foreach ($quantityList as $quantity) 
								        <option value="{{ $quantity['id'] }}">{{ $quantity['name'] }}</option>
								    @endforeach
								</select>
							</div>
						</div>
						<!--/span-->
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Quantity<span style="color: red">*</span></label>
								<input type="text" class="form-control" placeholder="Enter Quantity" name="quantity">
							</div>
						</div>
					</div>
				</div>
				<div class="form-actions right">
					<a href="{{url('admin/product')}}" class="btn default">Cancel</a>
					<button type="button" data-request="ajax-submit" data-target='[role="add-product"]' class="btn blue"><i class="fa fa-check"></i> Save</button>
				</div>
			</form>
			<!-- END FORM-->
		</div>		
	</div>
</div>
@push('scripts')
<script type="text/javascript">
	$(document).on('change', '#category_id', function(){
		var category_id = $(this).val();   
		$.ajax({
	        url: "{{url('admin/subcategory/getSubcategory')}}",
	        type: "POST",
	        data: {'category_id': category_id},
	        success: function (responce){
	        	$('#sub_category_id').html('');
        		$('#sub_category_id').append("<option>Select Subcategory</option>");
	        	$.each(responce.data, function(index, element) {
		            $('#sub_category_id').append("<option value='"+element.id+"'>"+element.name+"</option>");
		        });
        	}
     	});
	});
</script>
@endpush




