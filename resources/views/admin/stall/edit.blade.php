<div class="page-content-wrapper">
	<div class="page-content">
		<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<form role="add-stall" action="{{url('admin/stall/'.___encrypt($stallDetails['user_id']))}}" method="POST" class="horizontal-form">
				<input type="hidden" name="_method" value="PUT">
				<input type="hidden" name="action" value="edit">
				<div class="form-body">
					<h3 class="form-section">Edit Stall<span class="pull-right" style="font-size: 12px;">All fields marked with <span style="color: red">*</span> are manadatory</span></h3>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Tender<span style="color: red">*</label>
								<select class="form-control" name="tender">
									<option value="">Select Tender</option>
									@foreach ($tenderList as $tender) 
								        <option  
										@php 
										if($stallDetails['tender_id'] ==  $tender['id'] ){
										@endphp
											 selected =
											 "selected";
											@php
										}
										@endphp value="{{ $tender['id'] }}">
										{{ $tender['name'] }} 
										</option>



								    @endforeach
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Stall Name<span style="color: red">*</span></label>
								<input type="text" name="name" class="form-control" placeholder="Enter Stall Name" value="{{ $stallDetails['name']}}">
							</div>
						</div>
						
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Email<span style="color: red">*</span></label>
								<input type="text" name="email" class="form-control" placeholder="Enter Email" value="{{ $stallDetails['email']}}">
							</div>
						</div>
						
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Aadhar No.<span style="color: red">*</span></label>
								<input type="text" name="aadhar_no" class="form-control" placeholder="Enter Aadhar Number" minlength="12" maxlength="12" value="{{ $stallDetails['stall']['aadhar_no']}}">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Pan Card No.<span style="color: red">*</span></label>
								<input type="text" name="pan_no" class="form-control" placeholder="Enter Pan Card Number" minlength="10" maxlength="10" value="{{ $stallDetails['stall']['pan_no']}}">
							</div>
						</div>						
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">Country Code</label>
										<select class="form-control" name="country_code">
											<option value="+91">+91</option>
										</select>
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group">
										<label class="control-label">Mobile Number<span style="color: red">*</span></label>
										<input type="text" name="mobile" class="form-control" placeholder="Enter Mobile Number" minlength="10" maxlength="10" value="{{ $stallDetails['stall']['mobile']}}">
									</div>
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">Country Code</label>
										<select class="form-control" name="country_code">
											<option value="+91">+91</option>
										</select>
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group">
										<label class="control-label">Alternate Mobile Number</label>
										<input type="text" name="alternate_phone" class="form-control" placeholder="Enter Mobile Number" minlength="10" maxlength="10" value="{{ $stallDetails['alternate_phone']}}">
									</div>
								</div>
							</div>
						</div>
						
						<!--/span-->
					</div>
					
					
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Photo<span style="color: red">*</span></label>
								<input type="file" class="form-control" name="profile_picture">
								<input type="hidden" name="old_profile_picture" value="{{$stallDetails['stall']['profile_picture']}}">
							</div>
						</div>
					</div>
					
					<h3 class="form-section">Address</h3>
					<div class="row">
						<div class="col-md-12 ">
							<div class="form-group">
								<label>Street<span style="color: red">*</span></label>
								<input type="text" class="form-control" placeholder="Enter Street" name="street" value="{{ $stallDetails['address']['street']}}">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>City<span style="color: red">*</span></label>
								<input type="text" class="form-control" placeholder="Enter City" name="city" value="{{ $stallDetails['address']['city']}}">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label>State<span style="color: red">*</span></label>
								<input type="text" class="form-control" placeholder="Enter State" name="state" value="{{ $stallDetails['address']['state']}}">
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Post Code<span style="color: red">*</span></label>
								<input type="text" class="form-control" placeholder="Enter Post Code" name="pin_code" value="{{ $stallDetails['address']['pin_code']}}">
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label>Country<span style="color: red">*</span></label>
								<select class="form-control" name="country">
									<option value="India">India</option>
									<option value="Singapore">Singapore</option>
								</select>
							</div>
						</div>
						<!--/span-->
					</div>
					
				</div>
				<div class="form-actions right">
					<a href="{{url('admin/stall')}}" class="btn default">Cancel</a>
					<button type="button" data-request="ajax-submit" data-target='[role="add-stall"]' class="btn blue"><i class="fa fa-check"></i> Save</button>
				</div>
			</form>
			<!-- END FORM-->
		</div>		
	</div>
</div>