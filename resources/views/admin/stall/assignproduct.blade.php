<link href="{{ asset('assets/admin/pages/css/todo.css') }}" rel="stylesheet" type="text/css"/>
<link id="style_color" href="{{asset('assets/admin/layout4/css/themes/light.css')}}" rel="stylesheet" type="text/css"/>
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Assign Products <small>Assign products to the stall - {{ $stallDetails['name'] }}</small></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="{{ url('admin') }}">Home</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="{{ url('admin/stall') }}">Stalls</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<span style="color:#9eacb4;">Assign products</span>
			</li>
		</ul>
		<!-- END PAGE BREADCRUMB -->
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN TODO SIDEBAR -->
				<div class="todo-sidebar">
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption" data-toggle="collapse" data-target=".todo-project-list-content">
								<span class="caption-subject font-green-sharp bold uppercase">All Products </span>
								<span class="caption-helper visible-sm-inline-block visible-xs-inline-block">click the box to assign</span>
							</div>
						</div>
						<div class="portlet-body todo-project-list-content">
							<div class="todo-project-list">
								<ul class="nav nav-pills nav-stacked">

									@foreach($productList as $product)
									<li>
										<span class="pull-right" style="margin-top:12px;">
											<div class="md-checkbox">
												<input type="checkbox" id="checkbox_{{ $product['id']}}" class="md-check product_check" value="{{ $product['id'] }}" name=""
												@if(in_array($product['id'], $assignedProducts))
													checked
												@endif 
												>
												<label for="checkbox_{{ $product['id']}}">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>
												</label>
											</div>
										</span>
										<img src="{{ url('images/product')}}/{{ $product['image'] }}" class="todo-userpic pull-left" width="50" height="50">
										<span style="margin-left: 10px; font-size: 13px; font-weight: bold; ">{{ $product['name'] }}</span>
										<br />
										<span style="margin-left: 10px;">Left in stock - {{ $product['quantity'] }}</span>
									</li>
									<br />
									@endforeach
									<!-- <li class="active">
										<a href="javascript:;">
										<span class="badge badge-success badge-active"> 3 </span> GlobalEx System </a>
									</li> -->
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- END TODO SIDEBAR -->
				<!-- BEGIN TODO CONTENT -->
				<div class="todo-content">
					<div class="portlet light">
						<!-- PROJECT HEAD -->
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-bar-chart font-green-sharp hide"></i>
								<span class="caption-helper">Stall:</span> &nbsp; <span class="caption-subject font-green-sharp bold uppercase">{{ $stallDetails['name'] }}</span>
							</div>
						</div>
						<!-- end PROJECT HEAD -->
						<div class="portlet-body">
							<div class="row" id="assignedProductHolder">
								@foreach($stallsProducts as $assigendproducts)
								<div class="col-md-4 col-sm-4" id="product_{{ $assigendproducts['product_id'] }}">
									<div class="portlet light">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-cutlery font-grey-gallery"></i>
												<span class="caption-subject bold font-grey-gallery uppercase">
												{{ $assigendproducts['product']['name']}} </span>
											</div>
											<div class="tools">
												<!-- <a href="" class="remove"></a> -->
											</div>
										</div>
										<div class="portlet-body" style="text-align: center;">
											<img src="{{ asset('images/product')}}/{{ $assigendproducts['product']['image'] }}" width="80" height="80" style="border-radius: 50%;">
											<!-- <h4>{{ $assigendproducts['product']['name']}}</h4> -->
											<p>
												{{ substr($assigendproducts['product']['description'], 0, 30)}} . . 
											</p>
											Quantity: 
											<input type="text" id="quantity_{{ $assigendproducts['product_id'] }}_{{ $assigendproducts['stall_id'] }}" value="{{ $assigendproducts['quantity'] }}" style="width: 60px;text-align: center;" class="change_quantity 
											@if($assigendproducts['quantity'] < 10)
												pulsate
											@endif
											">
										</div>
									</div>
								</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
				<!-- END TODO CONTENT -->
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>


<script type="text/javascript">
	$(document).on('change', '.product_check', function() {
		var checked 		= $(this).prop("checked");
		var stall_id 		= '{{ $stallDetails["id"] }}';
		var product_id 		= $(this).val();
		var $target  		= $('#product_'+product_id);
		if (checked) {		//when checkbox is checked so product will be assigned
			$.ajax({
				type: 'POST',
				url: '{{ url("admin/stall/saveproduct") }}',
				data: {stall_id: stall_id, product_id: product_id},
				success: function(r) {
					var product = r.data.product;
					var stall = r.data.stall;
					var public_dir = "{{ asset('images/product')}}";
					var class_name = '';
					if (r.data.quantity < 10) {
						class_name = 'pulsate';
					}
					var $html = '<div style="display:none;" class="col-md-4 col-sm-4" id="product_'+product_id+'"><div class="portlet light"><div class="portlet-title"><div class="caption"> <i class="fa fa-cutlery font-grey-gallery"></i> <span class="caption-subject bold font-grey-gallery uppercase"> '+product.name+' </span></div><div class="tools"> </div></div><div class="portlet-body" style="text-align: center;"> <img src="'+public_dir+'/'+product.image+'" width="80" height="80" style="border-radius: 50%;"><p> '+product.description.substr(0, 30)+'. .</p><input type="text" id="quantity_'+product_id+'_'+stall_id+'" value="'+r.data.quantity+'" style="width: 60px;text-align: center;" class="change_quantity '+class_name+'"></div></div></div>';
					$('#assignedProductHolder').append($html);
					$('#product_'+product_id).show('slow');
				},
				error: function() {

				}
			});
		} else { 			//when checkbox is unchecked so product will be removed
			var stall_id = '{{ $stallDetails["id"] }}';
			var product_id = $(this).val();
			$.ajax({
				type: 'POST',
				url: '{{ url("admin/stall/removeproduct") }}',
				data: {stall_id: stall_id, product_id: product_id},
				success: function(r) {
					$target.hide('slow');
					$target.remove();
				},
				error: function() {

				}
			});
		}
	});
	$(document).on('change', '.change_quantity', function() {
		var id 		= this.id;
		var value 	= this.value;
		var res 	= id.split("_");
		var product_id = res['1'];
		var stall_id   = res['2'];
		$.ajax({
			type: 'POST',
			data: {quantity: value, product_id: product_id},
			url: '{{ url("admin/stall/checkquantity") }}',
			success: function(r) {
				if(r.data.available == 1) {
					$.ajax({
						type: 'POST',
						url: '{{ url("admin/stall/changequantity") }}',
						data: {product_id: product_id, stall_id: stall_id, quantity: value},
						success: function(r) {
							swal('', 'Quantity updated successfully', 'success')
							.then((value) => {
							  location.reload();
							});
						}
					});
				} else {
					swal('Not available', "Sorry! Product quantity is not available", 'error')
					.then((value) => {
						location.reload();
					})
				}
			}
		});
	});
</script>

@push('scripts')
<script type="text/javascript">
	var handlePulsate = function () {
        if (!jQuery().pulsate) {
            return;
        }

        if (Metronic.isIE8() == true) {
            return; // pulsate plugin does not support IE8 and below
        }

        if (jQuery().pulsate) {
            jQuery('.pulsate').pulsate({
                color: "#bf1c56"
            });

            jQuery('#pulsate-once').click(function () {
                $('#pulsate-once-target').pulsate({
                    color: "#399bc3",
                    repeat: false
                });
            });

            jQuery('#pulsate-crazy').click(function () {
                $('#pulsate-crazy-target').pulsate({
                    color: "#fdbe41",
                    reach: 50,
                    repeat: 10,
                    speed: 100,
                    glow: true
                });
            });
        }
    }
    handlePulsate();
</script>
@endpush