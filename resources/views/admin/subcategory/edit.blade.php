<div class="page-content-wrapper">
	<div class="page-content">
		<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<form role="update-subcategory" action="{{url('admin/subcategory/'.___encrypt($subcategoryDetails['id']))}}" method="POST" class="horizontal-form">
				<div class="form-body">
					<input type="hidden" name="_method" value="PUT">
					<input type="hidden" name="action" value="edit">
					<h3 class="form-section">{{$page_title}}<span class="pull-right" style="font-size: 12px;">All fields marked with <span style="color: red">*</span> are manadatory</span></h3>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Category<span style="color: red">*</span></label>
								<select class="form-control" name="category_id">
									<option value="">Select Category</option>
									@foreach ($categoryList as $category)
										<option value="{{ $category['id'] }}"
										@if($subcategoryDetails['category_id'] ==  $category['id'] ){
											 selected ="selected"
										@endif
										>
										{{ $category['name'] }} 
										</option>
								    @endforeach
								</select>
							</div>
						</div>
					</div>
					

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Subcategory Name<span style="color: red">*</span></label>
								<input type="text" id="subcategoryname" name="subcategory_name" value="{{ $subcategoryDetails['name'] }}" class="form-control" placeholder="Enter Subcategory Name">
								
							</div>
						</div>
					</div>
					
					
				</div>
				<div class="form-actions right">
					<a href="{{url('admin/subcategory')}}" class="btn default">Cancel</a>
					<button type="button" data-request="ajax-submit" data-target='[role="update-subcategory"]' class="btn blue"><i class="fa fa-check"></i> Save</button>
				</div>
			</form>
			<!-- END FORM-->
		</div>		
	</div>
</div>