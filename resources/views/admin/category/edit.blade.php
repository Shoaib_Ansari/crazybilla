<div class="page-content-wrapper">
	<div class="page-content">
		<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<form role="update-category" action="{{url('admin/category/'.___encrypt($categoryDetails['id']))}}" method="POST" class="horizontal-form">
				<div class="form-body">
					<input type="hidden" name="_method" value="PUT">
					<input type="hidden" name="action" value="edit">
					<h3 class="form-section">{{$page_title}}<span class="pull-right" style="font-size: 12px;">All fields marked with <span style="color: red">*</span> are manadatory</span></h3>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Category Name<span style="color: red">*</span></label>
								<input type="text" id="categoryname" value="{{$categoryDetails['name']}}" name="category_name" class="form-control" placeholder="Enter Category Name">
								
							</div>
						</div>
				</div>
				<div class="form-actions right">
					<a href="{{url('admin/category')}}" class="btn default">Cancel</a>
					<button type="button" data-request="ajax-submit" data-target='[role="update-category"]' class="btn blue"><i class="fa fa-check"></i> Save</button>
				</div>
			</form>
			<!-- END FORM-->
		</div>		
	</div>
</div>