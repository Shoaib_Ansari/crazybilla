<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStallsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('stalls', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tender_id');
			$table->integer('user_id');
			$table->string('name', 191);
			$table->string('phone', 191);
			$table->string('alternate_phone', 191);
			$table->string('email', 191);
			$table->enum('status', array('active','inactive','trashed'));
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('stalls');
	}

}
