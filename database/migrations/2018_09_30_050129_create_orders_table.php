<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('order_id', 191);
			$table->integer('stall_id');
			$table->integer('customer_id');
			$table->integer('address_id');
			$table->dateTime('order_time');
			$table->dateTime('delivery_time');
			$table->float('total_price');
			$table->enum('status', array('new','accepted','preparing','ontheway','addressnotfound','delivered','failed','completed'));
			$table->timestamps();
			$table->float('gst');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
