<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 191);
			$table->text('description', 65535);
			$table->text('image', 65535);
			$table->text('thumb', 65535);
			$table->float('price');
			$table->float('discount_price')->default(0.00);
			$table->string('discount_percent', 191)->default('');
			$table->integer('sku');
			$table->integer('quantity');
			$table->integer('quantity_type');
			$table->integer('category_id');
			$table->integer('subcategory_id');
			$table->enum('status', array('active','inactive','trashed'));
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
