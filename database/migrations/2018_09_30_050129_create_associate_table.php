<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAssociateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('associate', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 191);
			$table->string('user_id', 191);
			$table->string('email', 191)->unique();
			$table->string('first_name', 191);
			$table->string('last_name', 191);
			$table->string('country_code', 191);
			$table->string('mobile_number', 191);
			$table->string('street', 191);
			$table->string('city', 191);
			$table->string('state', 191);
			$table->string('country', 191);
			$table->string('post_code', 191);
			$table->string('gender', 191);
			$table->string('dob', 191);
			$table->enum('status', array('active','inactive','pending','trashed'))->default('active');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('associate');
	}

}
