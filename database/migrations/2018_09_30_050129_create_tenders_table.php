<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTendersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tenders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 191);
			$table->integer('investor_id')->nullable();
			$table->integer('added_by');
			$table->integer('user_id');
			$table->integer('category_id')->nullable();
			$table->integer('subcategory_id')->nullable();
			$table->integer('availability');
			$table->bigInteger('alternate_phone')->nullable();
			$table->enum('status', array('active','inactive','trashed'));
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tenders');
	}

}
