<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('mobile', 191);
			$table->string('name', 191);
			$table->string('full_name', 191);
			$table->string('email', 191);
			$table->string('country_code', 191);
			$table->string('aadhar_no', 191);
			$table->string('pan_no', 191);
			$table->string('password', 191);
			$table->enum('status', array('active','inactive','pending','trashed'))->default('active');
			$table->enum('user_type', array('admin','associate','investor','tender','customer','stall'))->default('customer');
			$table->string('otp', 191);
			$table->string('profile_picture', 191);
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
