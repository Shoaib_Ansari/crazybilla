<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersAddressTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_address', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('country', 191);
			$table->string('state', 191);
			$table->string('city', 191);
			$table->string('street', 191);
			$table->string('latitude', 191);
			$table->string('longitude', 191);
			$table->enum('address_type', array('primary','secondary'))->default('primary');
			$table->enum('status', array('active','inactive','deleted'))->default('active');
			$table->timestamps();
			$table->string('pin_code', 191);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_address');
	}

}
