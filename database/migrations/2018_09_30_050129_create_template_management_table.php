<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTemplateManagementTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('template_management', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('keywords', 191);
			$table->string('template_title', 191);
			$table->string('template_subject', 191);
			$table->string('template_description', 191);
			$table->enum('status', array('active','inactive','deleted'))->default('active');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('template_management');
	}

}
