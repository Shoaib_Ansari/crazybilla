<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductQuantityTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_quantity_types', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 191);
			$table->string('quantity_text', 191)->nullable();
			$table->string('short_text', 191)->nullable();
			$table->enum('status', array('active','inactive','trashed'))->default('active');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_quantity_types');
	}

}
