<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersBankDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_bank_details', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('bank_id');
			$table->string('bank_account', 191);
			$table->string('ifsc_code', 191);
			$table->string('bank_address', 191);
			$table->string('acc_holdername', 191);
			$table->enum('account_type', array('primary','secondary'))->default('primary');
			$table->enum('status', array('active','inactive','deleted'))->default('active');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_bank_details');
	}

}
