<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvestorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('investors', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('associate_id');
			$table->integer('user_id');
			$table->string('name', 191);
			$table->string('father_name', 191);
			$table->bigInteger('phone');
			$table->bigInteger('alternate_phone');
			$table->string('email', 191);
			$table->enum('email_verified', array('0','1'))->default('0');
			$table->enum('gender', array('male','female','other'));
			$table->enum('status', array('active','inactive','trashed'));
			$table->integer('invested_amount');
			$table->integer('current_balance');
			$table->dateTime('invest_date');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('investors');
	}

}
