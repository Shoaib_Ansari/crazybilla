<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;
use Image;
use DB;
use Auth;
use App\Quotation;
use Validations\Product as Validations;

class ProductController extends Controller
{
    public function __construct(Request $request){
        parent::__construct($request);
        $this->middleware('auth');
    }

     public function index(Request $request, Builder $builder){
        $data['site_title'] = $data['page_title'] = 'Product List';
        $data['menu']       = 'product-list';
        $data['breadcrumb'] = '<ul class="page-breadcrumb breadcrumb"><li><a href="'.url('/').'"><i class="fa fa-home" style="font-size:12px;"></i> Home</a><i class="fa fa-circle"></i></li><li> &nbsp;<a href="">Product</a></li></ul>';
        $data['view'] = 'admin.product.list';
        
        DB::statement(DB::raw('set @id=0'));
        $users = \Models\Product::select(DB::raw('@id := 0 r'))
                    ->select(DB::raw('@id := @id + 1 AS ids'),'products.*')->where('status', '<>', 'trashed');
        // $users  = _arefy(\Models\Users::where('status','!=','trashed')->where('user_type','=','associate')->get());
        
        if ($request->ajax()) {
            return DataTables::of($users)
            ->editColumn('action',function($item){
                $html    = '<div class="edit_details_box">';
                $html   .= '<a href="'.url(sprintf('admin/product/%s',___encrypt($item['id']))).'" title="View Detail"><i class="fa  fa-eye"></i></a> | ';
                $html   .= '<a href="'.url(sprintf('admin/product/%s/edit',___encrypt($item['id']))).'"  title="Edit Detail"><i class="fa fa-edit"></i></a> | ';
                if($item['status'] == 'active'){
                    $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/product/status/?id=%s&status=inactive',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/inactive-user.png').'"
                        data-ask="Would you like to change '.$item['name'].' status from active to inactive?" title="Update Status"><i class="fa fa-fw fa-ban"></i></a> | ';
                }elseif($item['status'] == 'inactive'){
                    $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/product/status/?id=%s&status=active',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to change '.$item['name'].' status from inactive to active?" title="Update Status"><i class="fa fa-fw fa-check"></i></a> | ';
                }elseif($item['status'] == 'pending'){
                    $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/product/status/?id=%s&status=active',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to change '.$item['name'].' status from pending to active?" title="Update Status"><i class="fa fa-fw fa-check"></i></a> | ';
                }
                $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/product/status/?id=%s&status=trashed',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to delete '.$item['name'].' record?" title="Delete Status"><i class="fa fa-trash"></i></a>';

                $html   .= '</div>';
                                
                return $html;
            })
            ->editColumn('image', function($item) {
                $html = '<a class="imagelight" href="'.url('/images/product/'.$item['image']).'"><img style="border-radius:50%;" src="'.url('/images/product/'.$item['image']).'" alt="'.$item['name'].'" width="80" height="80"></a>';
                return $html;
            })
            ->editColumn('status',function($item){
                return ucfirst($item['status']);
            })
             ->editColumn('name',function($item){
                return ucfirst($item['name']);
            })
            ->rawColumns(['action', 'image'])
            ->make(true);

        }

        $data['html'] = $builder
            ->parameters([
                "dom" => "<'row' <'col-md-6 col-sm-12 col-xs-4'l><'col-md-6 col-sm-12 col-xs-4'f>><'row filter'><'row white_box_wrapper database_table table-responsive'rt><'row' <'col-md-6'i><'col-md-6'p>>",
            ])

            ->addColumn(['data' => 'ids', 'name' => 'id','title' => 'S.No.','orderable' => true, 'width' => 40])
            ->addColumn(['data' => 'image', 'name' => 'image','title' => 'Product Image','orderable' => false, 'width' => 120])
            ->addColumn(['data' => 'name', 'name' => 'name','title' => 'Name','orderable' => true, 'width' => 120])
            ->addColumn(['data' => 'status','name' => 'status','title' => 'Status','orderable' => true, 'width' => 120])
            ->addAction(['title' => 'Action', 'orderable' => false, 'width' => 120]);
        return view('home')->with($data);
    }

    public function create(Request $request){
        $data['site_title'] = $data['page_title'] = 'Create Product';
        $data['menu']       = 'product-add';
        $data['categoryList'] = _arefy(\Models\Category::where('status','=','active')->select('id', 'name')->get());
        $data['quantityList'] = _arefy(\Models\product_quantity_type::where('status','=','active')->select('id', 'name')->get());
    	$data['view'] = 'admin/product/add';
    	return view('home',$data);
    }


    public function store(Request $request){
     	$validation = new Validations($request);
        $validator  = $validation->createProduct();
       	if($validator->fails()){
            $this->message              = $validator->errors();
        } else {
            $data['category_id']        = $request->category_id;
            $data['subcategory_id']     = $request->sub_category_id;
            $data['name']               = $request->productname;
            $data['description']        = $request->description;
            
            $data['price']              = $request->price;
            if ($request->image) {
                $image                  = $request->file('image');
                $input['imagename']     = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath        = 'images/product/';
                $upload                 = upload_file($request, 'image', $destinationPath, true);
                // $image->move($destinationPath, $input['imagename']);
                $data['image']          = $upload['filename'];
                $data['thumb']          = $upload['filename'];
                // dd($upload); 
            }
            $data['discount_price']     = $request->discount_price;
            $data['discount_percent']   = $request->discount_percent;
            $data['sku']                = 123;
            $data['quantity']           = $request->quantity;
            $data['quantity_type']      = $request->quantity_type;
            $data['status']             = 'active';
            $data['updated_at']         = date('Y-m-d H:i:s');
            $data['created_at']         = date('Y-m-d H:i:s');
            \Models\Product::add($data);
			$this->status               = true;
            $this->modal                = true;
            $this->alert                = true;
            $this->message              = "Product has been added successfully.";	
            $this->redirect             = url('admin/product');
        }
        return $this->populateresponse();
    }




    public function edit($id) {
        $data['site_title'] = $data['page_title'] = 'Edit Product';
        $data['menu']       = 'product-edit';
        $data['view']       = 'admin.product.edit';
        $id                 = ___decrypt($id);
        $data['categoryList'] = _arefy(\Models\Category::where('status','=','active')->select('id', 'name')->get());
        $data['quantityList'] = _arefy(\Models\product_quantity_type::where('status','=','active')->select('id', 'name')->get());
        $data['productDetails'] = _arefy(\Models\Product::list('single',$id));
        $data['subcategoryList'] = _arefy(\Models\Subcategory::where('category_id', '=', $data['productDetails']['category_id'])->select('id', 'name')->get());
        return view('home',$data);
    }



    public function update(Request $request, $id)
    { 
        $id = ___decrypt($id); 
        $request->id = $id;
        $validation = new Validations($request);
        $validator  = $validation->createProduct('edit');
        if($validator->fails()){
            $this->message              = $validator->errors();
        } else {
            $data['category_id']        = $request->category_id;
            $data['subcategory_id']     = $request->sub_category_id;
            $data['name']               = $request->productname;
            $data['description']        = $request->description;
            $data['price']              = $request->price;
            if ($request->image) {
                $image                  = $request->file('image');
                $input['imagename']     = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath        = 'images/product/';
                $upload                 = upload_file($request, 'image', $destinationPath, true);
                // $image->move($destinationPath, $input['imagename']);
                $data['image']          = $upload['filename'];
                $data['thumb']          = $upload['filename'];
                // dd($upload); 
            } else {
                $data['image']          = $request->old_image;
                $data['thumb']          = $request->old_image;
            }
            $data['discount_price']     = $request->discount_price;
            $data['discount_percent']   = $request->discount_percent;
            $data['sku']                = 123;
            $data['quantity']           = $request->quantity;
            $data['quantity_type']      = $request->quantity_type;
            $data['status']             = 'active';
            $data['updated_at']         = date('Y-m-d H:i:s');
            $data['created_at']         = date('Y-m-d H:i:s');
            \Models\Product::change($id, $data);
            $this->status               = true;
            $this->modal                = true;
            $this->alert                = true;
            $this->message              = "Product has been updated successfully.";   
            $this->redirect             = url('admin/product');
        }
        return $this->populateresponse();
    }




    public function changeStatus(Request $request)
    {
    	$validation = new Validations($request);
        $validator = $validation->changeStatus();

        if($validator->fails()){
            $this->message = $validator->errors();
        }else{
            $userData                = ['status' => $request->status, 'updated_at' => date('Y-m-d H:i:s')];
            $isUpdate               = \Models\Product::change($request->id,$userData);
            \Models\Associate::change($request->id,$userData);

            if($isUpdate){
                if($request->status == 'trashed'){
                    $this->message = 'Deleted Associate successfully.';
                }else{
                    $this->message = 'Updated Associate successfully.';
                }
	            $userData = ['status' => $request->status, 'updated_at' => date('Y-m-d H:i:s')];
	            $isUpdate = \Models\product_quantity_type::change($request->id,$userData);
	            if($isUpdate){
	                $this->message = 'Updated Product successfully.';
	                $this->status = true;
	                $this->redirect = true;
	                $this->jsondata = [];
	            }
	        }
	        return $this->populateresponse();
    	}
	}


    public function trash(Request $request, Builder $builder){
        $data['site_title'] = $data['page_title'] = 'Trash Product List';
        $data['menu']       = 'trash-product';
        $data['breadcrumb'] = '<ul class="page-breadcrumb breadcrumb"><li><a href="'.url('/').'"><i class="fa fa-home" style="font-size:12px;"></i> Home</a><i class="fa fa-circle"></i></li><li> &nbsp;<a href="">Trash Product</a></li></ul>';
        $data['view'] = 'admin.trash.productlist';
        
        DB::statement(DB::raw('set @id=0'));
        $users = \Models\Product::select(DB::raw('@id := 0 r'))
                    ->select(DB::raw('@id := @id + 1 AS ids'),'products.*')->where('status', '=', 'trashed');
        // $users  = _arefy(\Models\Users::where('status','!=','trashed')->where('user_type','=','associate')->get());
        
        if ($request->ajax()) {
            return DataTables::of($users)
            ->editColumn('action',function($item){
                $html    = '<div class="edit_details_box">';
                $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/product/status/?id=%s&status=active',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to restore '.$item['name'].' record?" title="Restore"><i class="fa fa-undo" aria-hidden="true"></i></a> | ';
                
                $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/product/destroy/%s',___encrypt($item['id']))).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to permanently delete '.$item['name'].' record?" title="Permanent Delete"><i class="fa fa-trash"></i></a>';
                $html   .= '</div>';
                                
                return $html;
            })
            ->editColumn('image', function($item) {
                $html = '<a class="imagelight" href="'.url('/images/product/'.$item['image']).'"><img src="'.url('/images/product/'.$item['image']).'" alt="'.$item['name'].'" width="80" height="80" style="border-radius:50%;"></a>';
                return $html;
            })
            ->editColumn('status',function($item){
                return ucfirst($item['status']);
            })
             ->editColumn('name',function($item){
                return ucfirst($item['name']);
            })
            ->rawColumns(['action', 'image'])
            ->make(true);

        }

        $data['html'] = $builder
            ->parameters([
                "dom" => "<'row' <'col-md-6 col-sm-12 col-xs-4'l><'col-md-6 col-sm-12 col-xs-4'f>><'row filter'><'row white_box_wrapper database_table table-responsive'rt><'row' <'col-md-6'i><'col-md-6'p>>",
            ])

            ->addColumn(['data' => 'ids', 'name' => 'id','title' => 'S.No.','orderable' => true, 'width' => 40])
            ->addColumn(['data' => 'image', 'name' => 'image','title' => 'Product Image','orderable' => false, 'width' => 120])
            ->addColumn(['data' => 'name', 'name' => 'name','title' => 'Name','orderable' => true, 'width' => 120])
            ->addColumn(['data' => 'status','name' => 'status','title' => 'Status','orderable' => true, 'width' => 120])
            ->addAction(['title' => 'Action', 'orderable' => false, 'width' => 120]);
        return view('home')->with($data);
    }


    public function destroy($id)
    {
        $table_user = DB::table('products');

        if (!empty($id)) 
        {
            $id = ___decrypt($id);
            
            $table_user->where('id','=',$id);
            $isDeleted = $table_user->delete(); 
        
            if($isDeleted)
            {
                $this->message = 'Product deleted successfully.';
                $this->status = true;
                $this->redirect = url('product/trash'); 
                $this->jsondata = [];
            }
        }
        return $this->populateresponse();
    }


}
