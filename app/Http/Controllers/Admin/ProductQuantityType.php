<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;
use Image;
use DB;
use Auth;
use App\Quotation;
use Validations\Product as Validations;
class ProductQuantityType extends Controller
{


    public function __construct(Request $request){
        parent::__construct($request);
        $this->middleware('auth');
    }


    
    public function index(Request $request, Builder $builder){
        $data['site_title'] = $data['page_title'] = 'Product Quantity List';
        $data['menu']       = 'quantity-list';
        $data['breadcrumb'] = '<ul class="page-breadcrumb breadcrumb"><li><a href="'.url('/').'"><i class="fa fa-home" style="font-size:12px;"></i> Home</a><i class="fa fa-circle"></i></li><li> &nbsp;<a href="">Product Quantity</a></li></ul>';
        $data['view'] = 'admin.quantity.list';
        
        DB::statement(DB::raw('set @id=0'));
        $users = \Models\product_quantity_type::select(DB::raw('@id := 0 r'))
                    ->select(DB::raw('@id := @id + 1 AS ids'),'product_quantity_types.*')
                    ->with(['product' => function($q){
                        $q->select('*');
                        }])
                    ->where('status', '<>', 'trashed')->get();
        if ($request->ajax()) {
            return DataTables::of($users)
            ->editColumn('action',function($item){
                $html    = '<div class="edit_details_box">';
                
                $html   .= '<a href="'.url(sprintf('admin/qty_type/%s/edit',___encrypt($item['id']))).'"  title="Edit Detail"><i class="fa fa-edit"></i></a> | ';
                if($item['status'] == 'active'){
                    if(count($item['product']) == 0 )
                    {
                    $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/qty_type/status/?id=%s&status=inactive',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/inactive-user.png').'"
                        data-ask="Would you like to change '.$item['name'].' status from active to inactive?" title="Update Status"><i class="fa fa-fw fa-ban"></i></a> | ';
                    }
                    else
                    {
                        $html   .= '<a href="javascript:void(0);" title="Update" onclick="deletewarning();"><i class="fa fa-fw fa-ban"></i></a> |'; 
                    }
                }elseif($item['status'] == 'inactive'){
                    $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/qty_type/status/?id=%s&status=active',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to change '.$item['name'].' status from inactive to active?" title="Update Status"><i class="fa fa-fw fa-check"></i></a> | ';
                }
                if(count($item['product']) == 0 )
                    {
                    $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/qty_type/destroy/%s',___encrypt($item['id']))).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/inactive-user.png').'"
                        data-ask="Would you like to delete '.$item['name'].' record?" title="Delete">
                        <i class="fa fa-trash"></i></a>';
                    }
                    else
                    {
                         $html   .= ' <a href="javascript:void(0);" title="Delete" onclick="deletewarning();"><i class="fa fa-trash"></i></a>';
                    }

                $html   .= '</div>';
                                
                return $html;
            })
            ->editColumn('status',function($item){
                return ucfirst($item['status']);
            })
             ->editColumn('name',function($item){
                return ucfirst($item['name']);
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        $data['html'] = $builder
            ->parameters([
                "dom" => "<'row' <'col-md-6 col-sm-12 col-xs-4'l><'col-md-6 col-sm-12 col-xs-4'f>><'row filter'><'row white_box_wrapper database_table table-responsive'rt><'row' <'col-md-6'i><'col-md-6'p>>",
            ])
            ->addColumn(['data' => 'ids', 'name' => 'id','title' => 'S.No.','orderable' => true, 'width' => 20])
            ->addColumn(['data' => 'name', 'name' => 'name','title' => 'Name','orderable' => true, 'width' => 120])
            ->addColumn(['data' => 'status','name' => 'status','title' => 'Status','orderable' => true, 'width' => 120])
            ->addAction(['title' => 'Action', 'orderable' => false, 'width' => 120]);
        return view('home')->with($data);
    } 
    public function create(Request $request)
    {
    	$data['site_title'] = $data['page_title'] = 'Create Quantity Type';
        $data['menu']       = 'quantity-add';
    	$data['view'] = 'admin/quantity/add';
    	return view('home',$data);
    }
    public function store(Request $request)
    {
    	$validation = new Validations($request);
        $validator  = $validation->createQtyType();
        if($validator->fails()){
            $this->message = $validator->errors();
        }else{
            $data['name']=$request->name;
            $data['updated_at']=date('Y-m-d H:i:s');
            $data['created_at']=date('Y-m-d H:i:s');
            \Models\product_quantity_type::add($data);
          
            $this->status = true;
            $this->modal    = true;
            $this->alert    = true;
            $this->message  = "Quantity Type has been added successfully.";
            $this->redirect = url('admin/qty_type');
        }
        return $this->populateresponse();
    }
    public function edit($id)
    {  
        $data['site_title'] = $data['page_title'] = 'Edit Quantity Type';
        $data['menu']       = 'quantity-edit';
        $data['view'] = 'admin.quantity.edit';
        $id = ___decrypt($id);
        $data['QtyDetails'] = _arefy(\Models\product_quantity_type::list('single',$id));
       // dd($data['categoryDetails']);
        return view('home',$data);
    }
    public function update(Request $request, $id)
    {
        $request->id = $id = ___decrypt($id);
        $validation = new Validations($request);
        $validator  = $validation->createQtyType('edit');
        if($validator->fails()){
            $this->message = $validator->errors();
        }else{
            $data['name']=$request->name;
            $data['updated_at']=date('Y-m-d H:i:s');
            $isUpdated = \Models\product_quantity_type::change($id, $data);
           	if($isUpdated) { 
	            $this->status = true;
	            $this->modal    = true;
	            $this->alert    = true;
	            $this->message  = "Quantity type has been updated successfully.";
	            $this->redirect = url('admin/qty_type');
            }
        }
        return $this->populateresponse();
    }


    public function changeStatus(Request $request)
    {
    	$validation = new Validations($request);
        $validator = $validation->changeStatus();
        if($validator->fails()){
            $this->message = $validator->errors();
        }else{
            $userData = ['status' => $request->status, 'updated_at' => date('Y-m-d H:i:s')];
            $isUpdate = \Models\product_quantity_type::change($request->id,$userData);
            if($isUpdate){
                $this->message = 'Updated Quantity type successfully.';
                $this->status = true;
                $this->redirect = true;
                $this->jsondata = [];
            }
        }
        return $this->populateresponse();
    }


    public function destroy($id)
    { 
        $table_users = DB::table('product_quantity_types');

        if (!empty($id)) 
        {
            $id = ___decrypt($id);

            $table_users->where('id','=',$id);
            $isDeleted = $table_users->delete(); 
        
            if($isDeleted)
            {
                $this->message = 'Product quantity deleted successfully.';
                $this->status = true;
                $this->redirect = url('admin/qty_type'); 
                $this->jsondata = [];
            }
        }
        return $this->populateresponse();
    }
}