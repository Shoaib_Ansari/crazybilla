<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;
use Image;
use DB;
use Auth;
use App\Quotation;
use Validations\Associate as Validations;


class SubcategoryController extends Controller
{

    public function __construct(Request $request){
        parent::__construct($request);
        $this->middleware('auth');
    }

    
    public function index(Request $request, Builder $builder){
        $data['site_title'] = $data['page_title'] = 'Subcategory List';
        $data['menu']       = 'subcategory-list';
        $data['breadcrumb'] = '<ul class="page-breadcrumb breadcrumb"><li><a href="'.url('/').'"><i class="fa fa-home" style="font-size:12px;"></i> Home</a><i class="fa fa-circle"></i></li><li> &nbsp;<a href="">Subcategory</a></li></ul>';
        $data['view'] = 'admin.subcategory.list';
        
        DB::statement(DB::raw('set @id=0'));
        $users = \Models\Subcategory::select(DB::raw('@id := 0 r'))
        ->select(DB::raw('@id := @id + 1 AS ids'),'subcategories.*')
        ->with(['product' => function($q){
                        $q->select('*');
                        }])->get();
        if ($request->ajax()) {
            return DataTables::of($users)
            ->editColumn('action',function($item){
                $html    = '<div class="edit_details_box">';
                
                $html   .= '<a href="'.url(sprintf('admin/subcategory/%s/edit',___encrypt($item['id']))).'"  title="Edit Detail"><i class="fa fa-edit"></i></a> | ';
                if($item['status'] == 'active'){
                    if(count($item['product']) == 0 )
                    {
                    $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/subcategory/status/?id=%s&status=inactive',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/inactive-user.png').'"
                        data-ask="Would you like to change '.$item['name'].' status from active to inactive?" title="Update Status"><i class="fa fa-fw fa-ban"></i></a> | ';
                    }
                    else
                    {
                         $html   .= '<a href="javascript:void(0);" title="Update" onclick="deletewarning();"><i class="fa fa-fw fa-ban"></i></a> |'; 
                    }
                }elseif($item['status'] == 'inactive'){
                    $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/subcategory/status/?id=%s&status=active',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to change '.$item['name'].' status from inactive to active?" title="Update Status"><i class="fa fa-fw fa-check"></i></a> | ';
                }
                if(count($item['product']) == 0 )
                    {
                    $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/subcategory/destroy/%s',___encrypt($item['id']))).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/inactive-user.png').'"
                        data-ask="Would you like to delete '.$item['name'].' record?" title="Delete">
                        <i class="fa fa-trash"></i></a>';
                    }
                    else
                    {
                         $html   .= ' <a href="javascript:void(0);" title="Delete" onclick="deletewarning();"><i class="fa fa-trash"></i></a>';
                    }
                
                $html   .= '</div>';
                                
                return $html;
            })
            ->editColumn('status',function($item){
                return ucfirst($item['status']);
            })
            ->editColumn('category_id',function($item){
                return ucfirst($item['category']['name']);
            })
             ->editColumn('name',function($item){
                return ucfirst($item['name']);
            })
            ->rawColumns(['action'])
            ->make(true);

        }

        $data['html'] = $builder
            ->parameters([
                "dom" => "<'row' <'col-md-6 col-sm-12 col-xs-4'l><'col-md-6 col-sm-12 col-xs-4'f>><'row filter'><'row white_box_wrapper database_table table-responsive'rt><'row' <'col-md-6'i><'col-md-6'p>>",
            ])

            ->addColumn(['data' => 'ids', 'name' => 'id','title' => 'S.No.','orderable' => true, 'width' => 20])
            ->addColumn(['data' => 'name', 'name' => 'name','title' => 'Subcategory Name','orderable' => true, 'width' => 120])
            ->addColumn(['data' => 'category_id', 'name' => 'category_id','title' => 'Under Category','orderable' => true, 'width' => 120])
            ->addColumn(['data' => 'status','name' => 'status','title' => 'Status','orderable' => true, 'width' => 120])
            ->addAction(['title' => 'Action', 'orderable' => false, 'width' => 120]);
        return view('home')->with($data);
    } 

    public function create(Request $request)
    {
    	$data['site_title'] = $data['page_title'] = 'Create Subcategory';
        $data['menu']       = 'subcategory-add';
    	$data['categoryList']    = _arefy(\Models\Category::where('status','=','active')->select('id', 'name')->get());
    	$data['view'] = 'admin/subcategory/add';
    	return view('home',$data);
    }


    public function store(Request $request)
    {
    	$validation = new Validations($request);
        $validator  = $validation->createSubcategory();
        if($validator->fails()){
            $this->message = $validator->errors();
        }else{
        	$data['category_id']=$request->category_id;
            $data['name']=$request->subcategory_name;
            $data['created_by']=Auth::user()->id;
            $data['updated_at']=date('Y-m-d H:i:s');
            $data['created_at']=date('Y-m-d H:i:s');

            \Models\Subcategory::add($data);
          
            $this->status = true;
            $this->modal    = true;
            $this->alert    = true;
            $this->message  = "Subcategory has been added successfully.";
            $this->redirect = url('admin/subcategory');
        }
        return $this->populateresponse();
    }


    public function edit($id)
    {  
        $data['site_title'] = $data['page_title'] = 'Edit Subcategory';
        $data['menu']       = 'subcategory-edit';
        $data['view'] = 'admin.subcategory.edit';
        $id = ___decrypt($id);
        $data['subcategoryDetails'] = _arefy(\Models\Subcategory::list('single',$id));
        $data['categoryList']    = _arefy(\Models\Category::select('id', 'name')->get());
        return view('home',$data);
    }

    public function update(Request $request, $id)
    {
        $id = ___decrypt($id);
        $request->id = $id;
        $validation = new Validations($request);
        $validator  = $validation->editSubcategory('edit');
        if($validator->fails()){
            $this->message = $validator->errors();
        }else{
            $data['category_id']=$request->category_id;
            $data['name']=$request->subcategory_name;
            $isUpdated = \Models\Subcategory::change($id, $data);
           if($isUpdated) { 
                $this->status = true;
                $this->modal    = true;
                $this->alert    = true;
                $this->message  = "Subcategory has been updated successfully.";
                $this->redirect = url('admin/subcategory');
            } else {
                $this->status = true;
                $this->modal    = true;
                $this->alert    = true;
                $this->message  = "No changes made.";
                $this->redirect = url('admin/subcategory');
            }
        }
        return $this->populateresponse();
    }


    public function changeStatus(Request $request)
    {
    	$validation = new Validations($request);
        $validator = $validation->changeStatus();

        if($validator->fails()){
            $this->message = $validator->errors();
        }else{
            $userData = ['status' => $request->status, 'updated_at' => date('Y-m-d H:i:s')];
            $isUpdate = \Models\Subcategory::change($request->id,$userData);
            if($isUpdate){
                $this->message = 'Updated Subcategory successfully.';
                $this->status = true;
                $this->redirect = true;
                $this->jsondata = [];
            }
        }
        return $this->populateresponse();
    }


    public function getSubcategory(Request $request)
    {
        $CategoryId         =  $request->category_id;
        $getDetails         = \Models\Subcategory::list('array', $CategoryId);
        if($getDetails){
            $this->status   = true;
            $this->redirect = true;
            $this->jsondata = $getDetails;
        }
       
        return $this->populateresponse();
    }


    public function destroy($id)
    { 
        $table_users = DB::table('subcategories');

        if (!empty($id)) 
        {
            $id = ___decrypt($id);

            $table_users->where('id','=',$id);
            $isDeleted = $table_users->delete(); 
        
            if($isDeleted)
            {
                $this->message = 'Subcategory deleted successfully.';
                $this->status = true;
                $this->redirect = url('admin/subcategory'); 
                $this->jsondata = [];
            }
        }
        return $this->populateresponse();
    }
}
