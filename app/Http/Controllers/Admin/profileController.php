<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validations\Profile as Validations;

class ProfileController extends Controller
{
    
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(Request $request){
        parent::__construct($request);
        $this->middleware('auth');
    }



    public function index()
    {
    	$data['site_title'] = $data['page_title'] = 'Edit Profile';
        $data['profileId'] = Auth::user()->id;
        $data['menu']       = '-';

        $UserType = Auth::user()->user_type;

        if($UserType == 'admin')
        {
            $data['imgFolderName'] = 'image';
        }
        if($UserType == 'associate')
        {
            $data['imgFolderName'] = 'image';
        }
        if($UserType == 'investor')
        {
            $data['imgFolderName'] = 'investor';
        }
        if($UserType == 'tender')
        {
            $data['imgFolderName'] = 'tender';
        }

        $id = Auth::user()->id;
        $data['UserDetails'] = _arefy(\Models\Users::where('id',$id)->select('*')->get());
        $data['profileDetails'] = _arefy(\Models\Associate::where('user_id',$id)->select('*')->get());
    	$data['view'] = 'admin.profile.edit';
    	return view('home',$data);
    }


    public function update(Request $request , $id)
    {
        $userID = ___decrypt($id);
        $request->id = $userID;
        $validation = new Validations($request);
        $validator  = $validation->updatePassword('update');
        if($validator->fails()){
            $this->message = $validator->errors();
        }else{
            if (\Hash::check($request->currentpassword, \Auth::user()->password)) {
                $data['password']   =\Hash::make($request->password);
                $isUpdated = \Models\Users::change($userID, $data);
                if($isUpdated) { 
                    $this->status   = true;
                    $this->modal    = true;
                    $this->alert    = true;
                    $this->message  = "Password has been updated successfully.";
                    $this->redirect = url('admin/profile');
                }
            } else {
                $this->status   = true;
                $this->modal    = true;
                $this->alert    = true;
                $this->successimage  = url('images/fail.png');
                $this->message  = "Old password does not matched, try again.";
                $this->redirect = true;
            }
        }
            return $this->populateresponse();
    }


    public function profileUpdate(Request $request)
    {
        $userID = $request->id;

        $validation = new Validations($request);
        $validator  = $validation->updateProfile('update');
        if($validator->fails())
        {
            $this->message = $validator->errors();
        }
        else
        { 
            $data['full_name']=$request->full_name;
            $data['email']=$request->email;
            $data['mobile']=$request->mobile;

            if ($request->profile_picture) 
            {
                $image = $request->file('profile_picture');
                $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('images/admin');
                $image->move($destinationPath, $input['imagename']);
                $data['profile_picture'] = $input['imagename'];
            }
            else
            {
                $data['profile_picture'] = $request->old_profile_picture;  
            }
            // dd($data);
            $isUpdated = \Models\Users::change($userID, $data);

        if($isUpdated) { 
                $this->status   = true;
                $this->modal    = true;
                $this->alert    = true;
                $this->message  = "Profile has been updated successfully.";
                $this->redirect = url('admin/profile');
            } else {
                $this->status   = true;
                $this->modal    = true;
                $this->alert    = true;
                $this->message  = "Nothing to update.";
                $this->redirect = url('admin/profile');
            }
        }
        return $this->populateresponse();
    }
    
}
