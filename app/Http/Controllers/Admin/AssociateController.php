<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;
use Image;
use DB;
use App\Quotation;
use Validations\Associate as Validations;

class AssociateController extends Controller
{
    
    public function __construct(Request $request){
        parent::__construct($request);
        $this->middleware('auth');
    }

    

    public function index(Request $request, Builder $builder){
        $data['site_title'] = $data['page_title'] = 'Associate List';
        $data['menu']       = 'associate-list';
        $data['breadcrumb'] = '<ul class="page-breadcrumb breadcrumb"><li><a href="'.url('/').'"><i class="fa fa-home" style="font-size:12px;"></i> Home</a><i class="fa fa-circle"></i></li><li> &nbsp;<a href="">Associate</a></li></ul>';
        $data['view'] = 'admin.associate.list';
        
        DB::statement(DB::raw('set @id=0'));
        
        $users = \Models\Associate::select(DB::raw('@id := 0 r'))
                    ->select(DB::raw('@id := @id + 1 AS ids'),'associate.*')
                    ->with(['associate' => function($q){
                        $q->select('*');
                    }])
                    ->with(['investors' => function($q){
                        $q->select('*');
                    }])
                    ->where('status', '<>', 'trashed')->get();
                    
        if ($request->ajax()) {
            return DataTables::of($users)
            ->editColumn('action',function($item){
                $html    = '<div class="edit_details_box">';
                $html   .= '<a href="'.url(sprintf('admin/associate/%s',___encrypt($item['user_id']))).'" title="View Detail"><i class="fa  fa-eye"></i></a> | ';
                $html   .= '<a href="'.url(sprintf('admin/associate/%s/edit',___encrypt($item['user_id']))).'"  title="Edit Detail"><i class="fa fa-edit"></i></a> | ';
                if($item['status'] == 'active'){

                    if (count($item['investors']) == 0) 
                    {
                    $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/associate/status/?id=%s&associate_id=%s&status=inactive',$item['user_id'],
                            $item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/inactive-user.png').'"
                        data-ask="Would you like to change '.$item['name'].' status from active to inactive?" title="Update Status"><i class="fa fa-fw fa-ban"></i></a> | ';
                    }
                    else
                    {
                    $html   .= '<a href="javascript:void(0);" title="Update" onclick="deletewarning();"><i class="fa fa-fw fa-ban"></i></a> |';
                    }


                }elseif($item['status'] == 'inactive'){
                    $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/associate/status/?id=%s&associate_id=%s&status=active',$item['user_id'], $item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to change '.$item['name'].' status from inactive to active?" title="Update Status"><i class="fa fa-fw fa-check"></i></a> | ';
                }elseif($item['status'] == 'pending'){
                    $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/associate/status/?id=%s&associate_id=%s&status=active',$item['user_id'], $item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to change '.$item['name'].' status from pending to active?" title="Update Status"><i class="fa fa-fw fa-check"></i></a> | ';
                }
                if (count($item['investors']) == 0) {
                    $html   .= ' <a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/associate/status/?id=%s&associate_id=%s&status=trashed',$item['user_id'], $item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to delete '.$item['name'].' record?" title="Delete Status"><i class="fa fa-trash"></i></a>';
                } else {
                    $html   .= ' <a href="javascript:void(0);" title="Delete" onclick="deletewarning();"><i class="fa fa-trash"></i></a>';
                }
                
                $html   .= '</div>';
                                
                return $html;
            })
            ->editColumn('status',function($item){
                return ucfirst($item['status']);
            })
             ->editColumn('name',function($item){
                return ucfirst($item['name']);
            })
            ->rawColumns(['action'])
            ->make(true);

        }

        $data['html'] = $builder
            ->parameters([
                "dom" => "<'row' <'col-md-6 col-sm-12 col-xs-4'l><'col-md-6 col-sm-12 col-xs-4'f>><'row filter'><'row white_box_wrapper database_table table-responsive'rt><'row' <'col-md-6'i><'col-md-6'p>>",
            ])
            ->addColumn(['data' => 'ids', 'name' => 'id','title' => 'S.No.','orderable' => true, 'width' => 40])
            ->addColumn(['data' => 'name', 'name' => 'name','title' => 'Name','orderable' => true, 'width' => 120])
            ->addColumn(['data' => 'email', 'name' => 'email','title' => 'Email','orderable' => true, 'width' => 120])
            ->addColumn(['data' => 'mobile_number', 'name' => 'mobile','title' => 'Mobile Number','orderable' => true, 'width' => 120])
            ->addColumn(['data' => 'status','name' => 'status','title' => 'Status','orderable' => true, 'width' => 120])
            ->addAction(['title' => 'Action', 'orderable' => false, 'width' => 120]);
        return view('home')->with($data);
    }



    public function create(Request $request) {
        $data['site_title'] = $data['page_title'] = 'Create Associate';
        $data['menu']       = 'associate-list';
    	$data['view']       = 'admin/associate/add';
    	return view('home',$data);
    }



    public function store(Request $request) {

        $validation = new Validations($request);
        $validator  = $validation->createAssociate();
        if($validator->fails()){
            $this->message = $validator->errors();
        }else{
            $data['name']           = $request->first_name;
            $data['full_name']      = $request->first_name.' '.$request->last_name;
            $data['email']          = $request->email;
            $data['mobile']         = $request->mobile;
            $data['country_code']   = $request->country_code;
            $data['aadhar_no']      = $request->aadhar_no;
            $data['pan_no']         = $request->pan_no;

            if ($request->profile_picture) {
                $image                      = $request->file('profile_picture');
                $input['imagename']         = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath            = public_path('images/associate');
                $image->move($destinationPath, $input['imagename']);
                $data['profile_picture']    = $input['imagename'];
            }
           
            $data['password']               = \Hash::make($request->password);
            $data['status']                 = 'active';
            $data['user_type']='associate';
            $data['otp']=strtoupper(__random_string(6));
            $data['updated_at']=date('Y-m-d H:i:s');
            $data['created_at']=date('Y-m-d H:i:s');
            $inserId = \Models\Users::add($data);
           if($inserId){
            $associate['first_name']=$request->first_name;
            $associate['last_name']=$request->last_name;
            $associate['name'] = $request->first_name.' '.$request->last_name;
            $associate['email']=$request->email;
            $associate['mobile_number']=$request->mobile;
            $associate['country_code']=$request->country_code;
            $associate['gender']=$request->gender;
            $associate['dob']=$request->date_of_birth;
            $associate['user_id'] =$inserId; 
            $associate['status'] = 'active';
            $associate['city']=$request->city;
            $associate['state']=$request->state;
            $associate['country']=$request->country;
            $associate['post_code']=$request->pin_code;
            $associate['street']=$request->street;
            $associate['updated_at']=date('Y-m-d H:i:s');
            $associate['created_at']=date('Y-m-d H:i:s');
             \Models\Associate::add($associate);

            $associate_address['user_id']    = $inserId;
            $associate_address['city']       = $request->city;
            $associate_address['state']      = $request->state;
            $associate_address['country']    = $request->country;
            $associate_address['pin_code']   = $request->pin_code;
            $associate_address['street']     = $request->street;
            $associate_address['updated_at'] = date('Y-m-d H:i:s');
            $associate_address['created_at'] = date('Y-m-d H:i:s');

            \Models\UsersAddress::add($associate_address);

           } 
            $this->status   = true;
            $this->modal    = true;
            $this->alert    = true;
            $this->message  = "Associate has been added successfully.";
            $this->redirect = url('admin/associate');
        } 
        return $this->populateresponse();
    }

    public function show(Request $request, $id,  Builder $builder){

        $data['site_title'] = $data['page_title'] = 'View Associate';
        $data['menu']       = 'associate-list';
        $data['view']       = 'admin.associate.view';
        $id = ___decrypt($id);
        $data['associateDetails'] = _arefy(\Models\Associate::list('single',$id));
        $data['breadcrumb'] = '<span class="caption-subject font-green-sharp bold uppercase">All Investors</span>';
        $data['view'] = 'admin.associate.view';
        DB::statement(DB::raw('set @id=0'));
        $users = \Models\Investor::select(DB::raw('@id := 0 r'))
                    ->select(DB::raw('@id := @id + 1 AS ids'),'investors.*')->where('associate_id', $data['associateDetails']['id'])->where('status', '<>', 'trashed');
                
        if ($request->ajax()) {
            return DataTables::of($users)
            ->editColumn('action',function($item){
                $html    = '<div class="edit_details_box">';
                $html   .= '<a href="'.url(sprintf('admin/investor/%s',___encrypt($item['id']))).'" title="View Detail"><i class="fa  fa-eye"></i></a> | ';
                // $html   .= '<a href="'.url(sprintf('admin/investor/%s/edit',___encrypt($item['id']))).'"  title="Edit Detail"><i class="fa fa-edit"></i></a> | ';    
                if($item['status'] == 'active'){
                    $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/investor/status/?id=%s&status=inactive',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/inactive-user.png').'"
                        data-ask="Would you like to change '.$item['name'].' status from active to inactive?" title="Update Status"><i class="fa fa-fw fa-ban"></i></a> | ';
                }elseif($item['status'] == 'inactive'){
                    $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/investor/status/?id=%s&status=active',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to change '.$item['name'].' status from inactive to active?" title="Update Status"><i class="fa fa-fw fa-check"></i></a> | ';
                }elseif($item['status'] == 'pending'){
                    $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/investor/status/?id=%s&status=active',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to change '.$item['name'].' status from pending to active?" title="Update Status"><i class="fa fa-fw fa-check"></i></a> | ';
                }
                $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/investor/status/?id=%s&status=trashed',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to delete '.$item['name'].' record?" title="Delete Status"><i class="fa fa-trash"></i></a>';
                $html   .= '</div>';
                                
                return $html;
            })
            ->editColumn('status',function($item){
                return ucfirst($item['status']);
            })
             ->editColumn('name',function($item){
                return ucfirst($item['name']);
            })
             ->rawColumns(['action'])
             ->make(true);
        }

        $data['html'] = $builder
            ->parameters(["dom" => "<'row' <'col-md-6 col-sm-12 col-xs-4'l><'col-md-6 col-sm-12 col-xs-4'f>><'row filter'><'row white_box_wrapper database_table table-responsive'rt><'row' <'col-md-6'i><'col-md-6'p>>"])
            ->addColumn(['data' => 'ids', 'name' => 'id','title' => 'S.No.','orderable' => true, 'width' => 20])
            ->addColumn(['data' => 'name', 'name' => 'name','title' => 'Name','orderable' => true, 'width' => 120])
            ->addColumn(['data' => 'email', 'name' => 'email','title' => 'Email','orderable' => true, 'width' => 150])
            ->addColumn(['data' => 'phone', 'name' => 'phone','title' => 'Mobile Number','orderable' => true, 'width' => 120])
            ->addColumn(['data' => 'status','name' => 'status','title' => 'Status','orderable' => true, 'width' => 50]);
           // ->addAction(['title' => 'Action', 'orderable' => false, 'width' => 120]);
        return view('home')->with($data);
    }



    public function edit($id)
    {
        $data['site_title'] = $data['page_title'] = 'Edit Associate';
        $data['menu']       = 'associate-edit';
        $data['view']       = 'admin.associate.edit';
        $id                 = ___decrypt($id);
        $data['associateDetails'] = _arefy(\Models\Associate::list('single',$id));
        return view('home',$data);
    }


    
    public function update(Request $request, $id)
    { 
        $id = ___decrypt($id); 
        $request->id = $id;
        $validation = new Validations($request);
        If($request->updateProfile)
        {
            $validator  = $validation->updateProfile('update');
        }
        else
        {
            $validator  = $validation->createAssociate('edit');
        }
        
        if($validator->fails()){
            $this->message = $validator->errors();
        }else{
            $data['name']=$request->first_name;
            $data['full_name'] = $request->first_name.' '.$request->last_name;
            if ($request->email) 
            {
               $data['email']=$request->email;
            }
            $data['mobile']=$request->mobile;
            if ($request->country_code) 
            {
               $data['country_code']=$request->country_code;
            }
            if ($request->aadhar_no) 
            {
                $data['aadhar_no'] = $request->aadhar_no;
            }
            if ($request->pan_no) 
            {
                $data['pan_no'] = $request->pan_no;
            }
            

            if ($request->profile_picture) {
                $image = $request->file('profile_picture');
                $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('images/associate');
                $image->move($destinationPath, $input['imagename']);
                $data['profile_picture'] = $input['imagename'];
            }
            else
            {
              $data['profile_picture'] = $request->old_profile_picture;  
            }

            $data['password']   =\Hash::make($request->password);
            $data['status'] = 'active';
            $data['user_type']='associate';
            $data['otp']=strtoupper(__random_string(6));
            $data['updated_at']=date('Y-m-d H:i:s');
            // $data['created_at']=date('Y-m-d H:i:s');
           
           
            $associate['first_name']=$request->first_name;
            $associate['last_name']=$request->last_name;
            $associate['name'] = $request->first_name.' '.$request->last_name;
            if ($request->email) 
            {
                $associate['email']=$request->email;
            }
            $associate['mobile_number']=$request->mobile;
            if ($request->country_code) 
            {
                $associate['country_code']=$request->country_code;
            }
            
            $associate['gender']=$request->gender;
            $associate['dob']=$request->date_of_birth;
            $associate['status'] = 'active';
            $associate['city']=$request->city;
            $associate['state']=$request->state;
            $associate['country']=$request->country;
            $associate['post_code']=$request->pin_code;
            $associate['street']=$request->street;
            $associate['updated_at']=date('Y-m-d H:i:s');
            $isUpdated = \Models\Users::change($id, $data);
            \Models\Associate::change($id, $associate);

            $associate_address['city']       = $request->city;
            $associate_address['state']      = $request->state;
            $associate_address['country']    = $request->country;
            $associate_address['pin_code']   = $request->pin_code;
            $associate_address['street']     = $request->street;
            $associate_address['updated_at'] = date('Y-m-d H:i:s');
            $associate_address['created_at'] = date('Y-m-d H:i:s');
            \Models\UsersAddress::change($id, $associate_address);

            if($isUpdated) { 
                $this->status   = true;
                $this->modal    = true;
                $this->alert    = true;
                If($request->updateProfile)
                {
                    $this->message  = "Profile has been updated successfully.";
                    $this->redirect = url('associate/profile');
                }
                else
                {
                    $this->message  = "Associate has been updated successfully.";
                    $this->redirect = url('admin/associate'); 
                }

            }
        }
        return $this->populateresponse();
    }




    public function changeStatus(Request $request)
    {
        $validation = new Validations($request);
        $validator = $validation->changeStatus();

        if($validator->fails()){
            $this->message = $validator->errors();
        }
        else
        {
            
            $chkAssociateAssigned = _arefy(\Models\Investor::where('associate_id','=', $request->associate_id)->get());
            if(!empty($chkAssociateAssigned))
            {
                $this->message = 'You cannot change status due associate assigned.';
                $this->status = true;
                $this->redirect = true;
                return $this->populateresponse();
            }
            else
            {
                $userData = ['status' => $request->status, 'updated_at' => date('Y-m-d H:i:s')];
                $isUpdate = \Models\Users::change($request->id,$userData);
                \Models\Associate::change($request->id,$userData);

                if($isUpdate)
                {
                    if($request->status == 'trashed')
                    {
                        $this->message = 'Deleted Associate successfully.';
                    }
                    else
                    {
                        $this->message = 'Updated Associate successfully.';
                    }
                    $this->status = true;
                    $this->redirect = true;
                    $this->jsondata = [];
                }
                return $this->populateresponse();
            }
            
        }
        
    }



    public function profile() {
        $data['site_title'] = $data['page_title'] = 'Edit Profile';
        $data['profileId'] = \Auth::user()->id;
        $data['menu']       = '-';
        $id = \Auth::user()->id;
        $data['UserDetails'] = _arefy(\Models\Users::where('id',$id)->select('*')->get());
        $data['profileDetails'] = _arefy(\Models\Associate::where('user_id',$id)->select('*')->get());
        $data['view'] = 'admin.associate.profile';
        return view('home',$data);
    }

     public function trash(Request $request, Builder $builder){
        $data['site_title'] = $data['page_title'] = 'Trash Associate List';
        $data['menu']       = 'trash-associate';
        $data['breadcrumb'] = '<ul class="page-breadcrumb breadcrumb"><li><a href="'.url('/').'"><i class="fa fa-home" style="font-size:12px;"></i> Home</a><i class="fa fa-circle"></i></li><li> &nbsp;<a href="">Trash Associate</a></li></ul>';
        $data['view'] = 'admin.trash.associatelist';
        // $users  = _arefy(\Models\Users::where('status','!=','trashed')->where('user_type','=','associate')->get());
        
        DB::statement(DB::raw('set @id=0'));
        $users = \Models\Users::select(DB::raw('@id := 0 r'))
                    ->select(DB::raw('@id := @id + 1 AS ids'),'users.*')->where('status', '=', 'trashed')->where('user_type','associate');
        if ($request->ajax()) {
            return DataTables::of($users)
            ->editColumn('action',function($item){
                $html    = '<div class="edit_details_box">';
                
                $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/associate/status/?id=%s&status=active',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to restore '.$item['name'].' record?" title="Restore"><i class="fa fa-undo" aria-hidden="true"></i></a> | ';
                
                $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/associate/destroy/%s',___encrypt($item['id']))).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to permanently delete '.$item['name'].' record?" title="Permanent Delete"><i class="fa fa-trash"></i></a>';
                $html   .= '</div>';
                                
                return $html;
            })
            ->editColumn('status',function($item){
                return ucfirst($item['status']);
            })
             ->editColumn('name',function($item){
                return ucfirst($item['full_name']);
            })
            ->rawColumns(['action'])
            ->make(true);

        }

        $data['html'] = $builder
            ->parameters([
                "dom" => "<'row' <'col-md-6 col-sm-12 col-xs-4'l><'col-md-6 col-sm-12 col-xs-4'f>><'row filter'><'row white_box_wrapper database_table table-responsive'rt><'row' <'col-md-6'i><'col-md-6'p>>",
            ])

            ->addColumn(['data' => 'ids', 'name' => 'id','title' => 'S.No.','orderable' => true, 'width' => 40])
            ->addColumn(['data' => 'full_name', 'name' => 'name','title' => 'Name','orderable' => true, 'width' => 120])
            ->addColumn(['data' => 'email', 'name' => 'email','title' => 'Email','orderable' => true, 'width' => 120])
            ->addColumn(['data' => 'mobile', 'name' => 'mobile','title' => 'Mobile Number','orderable' => true, 'width' => 120])
            ->addColumn(['data' => 'status','name' => 'status','title' => 'Status','orderable' => true, 'width' => 120])
            ->addAction(['title' => 'Action', 'orderable' => false, 'width' => 120]);
        return view('home')->with($data);
    }



    public function destroy($id)
    {
        $table_users = DB::table('users');
        $table_user = DB::table('associate');
        $table_useraddress = DB::table('users_address');

        if (!empty($id)) 
        {
            $id = ___decrypt($id);

            $table_users->where('id','=',$id);
            $isDeleted = $table_users->delete(); 

            $table_user->where('user_id','=',$id);
            $table_user->delete(); 

            $table_useraddress->where('user_id','=',$id);
            $table_useraddress->delete(); 
        
            if($isDeleted)
            {
                $this->message = 'Associate deleted successfully.';
                $this->status = true;
                $this->redirect = url('associate/trash'); 
                $this->jsondata = [];
            }
        }
        return $this->populateresponse();
    }






}