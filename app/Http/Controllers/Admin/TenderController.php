<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;
use Validations\Tender as Validations;
use Auth;
use DB;

class TenderController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(Request $request){
        parent::__construct($request);
        $this->middleware('auth');
    }


    public function index(Request $request, Builder $builder){
        $data['site_title'] = $data['page_title'] = 'Tenders List';
        $data['menu']       = 'tender-list';
        $data['breadcrumb'] = '<ul class="page-breadcrumb breadcrumb"><li><a href="'.url('/').'"><i class="fa fa-home" style="font-size:12px;"></i> Home</a><i class="fa fa-circle"></i></li><li> &nbsp;<a href="">Tenders</a></li></ul>';
        $data['view'] = 'admin.tender.list';
       // $users  = _arefy(\Models\Users::where('status','!=','trashed')->where('user_type','=','tender')->get());

        DB::statement(DB::raw('set @id=0'));
        $users = \Models\Users::select(DB::raw('@id := 0 r'))
                    ->select(DB::raw('@id := @id + 1 AS ids'),'users.*')->where('status', '<>', 'trashed')->where('user_type','tender');

        if ($request->ajax()) {
            return DataTables::of($users)
            ->editColumn('action',function($item){
                $html    = '<div class="edit_details_box">';
                $html   .= '<a href="'.url(sprintf('admin/tender/%s',___encrypt($item['id']))).'" title="View Detail"><i class="fa fa-fw fa-eye"></i></a> | ';
                $html   .= '<a href="'.url(sprintf('admin/tender/%s/edit',___encrypt($item['id']))).'"  title="Edit Detail"><i class="fa fa-edit"></i></a> | ';
                if($item['status'] == 'active'){
                    $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/tender/status/?id=%s&status=inactive',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/inactive-user.png').'"
                        data-ask="Would you like to change '.$item['name'].' status from active to inactive?" title="Update Status"><i class="fa fa-fw fa-ban"></i></a> | ';
                }elseif($item['status'] == 'inactive'){
                    $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/tender/status/?id=%s&status=active',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to change '.$item['name'].' status from inactive to active?" title="Update Status"><i class="fa fa-fw fa-check"></i></a> | ';
                }elseif($item['status'] == 'pending'){
                    $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/tender/status/?id=%s&status=active',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would |  you like to change '.$item['name'].' status from pending to active?" title="Update Status"><i class="fa fa-fw fa-check"></i></a> | ';
                }
                $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/tender/status/?id=%s&status=trashed',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to delete '.$item['name'].' record?" title="Delete Status"><i class="fa fa-trash"></i></a>';
                $html   .= '</div>';
                                
                return $html;
            })
            ->editColumn('status',function($item){
                return ucfirst($item['status']);
            })
             ->editColumn('name',function($item){
                return ucfirst($item['name']);
            })
            ->rawColumns(['action'])
            ->make(true);
        }

        $data['html'] = $builder
            ->parameters([
                "dom" => "<'row' <'col-md-6 col-sm-12 col-xs-4'l><'col-md-6 col-sm-12 col-xs-4'f>><'row filter'><'row white_box_wrapper database_table table-responsive'rt><'row' <'col-md-6'i><'col-md-6'p>>",
            ])
            ->addColumn(['data' => 'ids', 'name' => 'id','title' => 'S.No.','orderable' => true, 'width' => 40])
            ->addColumn(['data' => 'name', 'name' => 'name','title' => 'Name','orderable' => false, 'width' => 120])
            ->addColumn(['data' => 'email', 'name' => 'email','title' => 'Email','orderable' => false, 'width' => 120])
            ->addColumn(['data' => 'status','name' => 'status','title' => 'Status','orderable' => false, 'width' => 120])
            ->addAction(['title' => 'Action', 'orderable' => false, 'width' => 120]);
        return view('home')->with($data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function create(Request $request){
        $data['site_title']         = $data['page_title'] = 'Create Tender';
        $data['menu']               = 'tender-add';
        $data['AvailabilityList']   = array(
                                                0 => array('id' => 1, 'name' => 'Day Shift'),
                                                1 => array('id' => 2, 'name' => 'Evening Shift')
                                            );
        $data['categoryList']       = _arefy(\Models\Category::get());
        $data['view']               = 'admin.tender.add';
        return view('home',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $validation = new Validations($request);
        $validator  = $validation->createtender();
        if($validator->fails()){
            $this->message = $validator->errors();
        }else{
            #Code to upload pic
            $image = $request->file('profile_picture');
            $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('images/tender');
            $image->move($destinationPath, $input['imagename']);
            $data['profile_picture'] = $input['imagename'];

            $data['name']               = $request->name;
            $data['full_name']          = $request->name;
            $data['email']              = $request->email;
            $data['mobile']             = $request->mobile;
            $data['country_code']       = '+91';
            $data['password']           = \Hash::make($request->password);
            $data['aadhar_no']          = $request->aadhar_no;
            $data['pan_no']             = $request->pan_no;
            $data['status']             = 'active';
            $data['user_type']          = 'tender';
            $data['otp']                = strtoupper(__random_string(6));
            $data['updated_at']         = date('Y-m-d H:i:s');
            $data['created_at']         = date('Y-m-d H:i:s');
            $inserId = \Models\Users::add($data);
            if($inserId){
                $tender['name']               = $request->name;
                $tender['added_by']           = Auth::user()->id;
                $tender['user_id']            = $inserId;
                /*$tender['category_id']      = $request->category_id,
                $tender['subcategory_id']     = $request->subcategory_id,*/
                $tender['alternate_phone']    = $request->alternate_phone;
                $tender['availability']       = $request->availability;
                $tender['status']             = 'active';
                $tender['updated_at']         = date('Y-m-d H:i:s');
                $tender['created_at']         = date('Y-m-d H:i:s');

                \Models\Tender::add($tender);

                $address_for_lat_long         = $request->city.' '.$request->state.' '.$request->country;
                $lat_long                     = _getGEOLocationByAddress($address_for_lat_long);

                $tender_address['user_id']    = $inserId;
                $tender_address['city']       = $request->city;
                $tender_address['state']      = $request->state;
                $tender_address['country']    = $request->country;
                $tender_address['pin_code']   = $request->pin_code;
                $tender_address['street']     = $request->street;
                $tender_address['latitude']   = $lat_long['lat'];         //'26.846695' for lucknow
                $tender_address['longitude']  = $lat_long['long'];        //'80.946167' for lucknow
                $tender_address['updated_at'] = date('Y-m-d H:i:s');
                $tender_address['created_at'] = date('Y-m-d H:i:s');

                \Models\UsersAddress::add($tender_address);
            }
            $this->status   = true;
            $this->modal    = true;
            $this->alert    = true;
            $this->message  = "Tender has been added successfully.";
            $this->redirect = url('admin/tender');
        } 
        return $this->populateresponse();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['site_title'] = $data['page_title'] = 'Edit Tender';
        $data['menu']       = 'tender-edit';
        $data['view'] = 'admin.tender.edit';
        $id = ___decrypt($id);
        $data['AvailabilityList']   = array(0 => array('id' => 1, 'name' => 'Day Shift'), 1 => array('id' => 2, 'name' => 'Evening Shift'));
        $data['tenderList'] = _arefy(\Models\Tender::list('single',$id));
        // pp($data['tenderList']);
        return view('home',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id                     = ___decrypt($id);
        $request->id            = $id;
        $validation             = new Validations($request);

        If($request->updateProfile)
        {
            $validator  = $validation->updateProfile('update');
        }
        else
        {
            $validator  = $validation->createTender('edit');
        }
        
        if($validator->fails()){
            $this->message      = $validator->errors();
        } else {
            $data['name']       = $request->name;
            $data['full_name']  = $request->name;
            if ($request->email) 
            {
                $data['email']  = $request->email;
            }
                $data['mobile']     = $request->mobile;
            if ($request->aadhar_no) 
            {
                $data['aadhar_no']  = $request->aadhar_no;
            }
            if ($request->pan_no) 
            {
                $data['pan_no']     = $request->pan_no;
            }

            if ($request->profile_picture) {
                $image = $request->file('profile_picture');
                $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('images/tender');
                $image->move($destinationPath, $input['imagename']);
                $data['profile_picture'] = $input['imagename'];
            } else {
                $data['profile_picture']    = $request->old_profile_picture;
            }

            if ($request->password) {
                $data['password']           = \Hash::make($request->password);
            }            
            $data['updated_at']             = date('Y-m-d H:i:s');
            $updatedInUser                  = \Models\Users::change($id, $data);

            $tender['name']                 = $request->name;
            if ($request->alternate_phone) 
            {
                $tender['alternate_phone']  = $request->alternate_phone;
            }
            if ($request->availability) 
            {
                $tender['availability']         = $request->availability;
            }
            $tender['updated_at']           = date('Y-m-d H:i:s');
            $updateInTender                 = \Models\Tender::change($id, $tender);

            $address_for_lat_long           = $request->city.' '.$request->state.' '.$request->country;
            $lat_long                       = _getGEOLocationByAddress($address_for_lat_long);

            $tender_address['city']       = $request->city;
            $tender_address['state']      = $request->state;
            $tender_address['country']    = $request->country;
            $tender_address['pin_code']   = $request->pin_code;
            $tender_address['street']     = $request->street;
            $tender_address['latitude']   = $lat_long['lat'];
            $tender_address['longitude']  = $lat_long['long'];
            $tender_address['updated_at'] = date('Y-m-d H:i:s');

            $updateInAddress                = \Models\UsersAddress::change($id, $tender_address);

            if($updatedInUser && $updateInTender && $updateInAddress) { 
                $this->status               = true;
                $this->modal                = true;
                $this->alert                = true;
                If($request->updateProfile)
                { 
                    $this->message              = "Profile has been updated successfully.";
                    $this->redirect             = url('tender/profile');
                }
                else
                {
                    $this->message              = "The Tender has been updated successfully.";
                    $this->redirect             = url('admin/tender');
                }
            } else {
                $this->status               = true;
                $this->modal                = true;
                $this->alert                = true;
                $this->message              = "There is some problem in the server right now, please try again after some time.";
            }
        }
        return $this->populateresponse();
    }



    public function changeStatus(Request $request)
    {
        $validation = new Validations($request);
        $validator = $validation->changeStatus();
        if($validator->fails()){
            $this->message          = $validator->errors();
        }else{
            $userData               = ['status' => $request->status, 'updated_at' => date('Y-m-d H:i:s')];
            $isUpdate               = \Models\Users::change($request->id,$userData);
            $isUpdate               = \Models\Tender::change($request->id,$userData);
            if($isUpdate){
                if($request->status == 'trashed'){
                    $this->message  = 'Deleted Tender successfully.';
                }else{
                    $this->message  = 'Updated Tender successfully.';
                }
                $this->status       = true;
                $this->redirect     = true;
                $this->jsondata     = [];
            }
        }
        return $this->populateresponse();
    }

   
    public function profile() {
        $data['site_title'] = $data['page_title'] = 'Edit Profile';
        $data['profileId'] = \Auth::user()->id;
        $data['menu']       = '-';
        $id = \Auth::user()->id;
        $data['UserDetails'] = _arefy(\Models\Users::where('id',$id)->select('*')->get());
        $data['profileDetails'] = _arefy(\Models\Tender::where('user_id',$id)->select('*')->get());
        $data['addressDetails'] = _arefy(\Models\UsersAddress::where('user_id',$id)->select('*')->get());
        $data['view'] = 'admin.tender.profile';
        return view('home',$data);
    }


    public function trash(Request $request, Builder $builder){
        $data['site_title'] = $data['page_title'] = 'Trash Tenders List';
        $data['menu']       = 'trash-tender';
        $data['breadcrumb'] = '<ul class="page-breadcrumb breadcrumb"><li><a href="'.url('/').'"><i class="fa fa-home" style="font-size:12px;"></i> Home</a><i class="fa fa-circle"></i></li><li> &nbsp;<a href="">Trash Tenders</a></li></ul>';
        $data['view'] = 'admin.trash.tenderlist';
       // $users  = _arefy(\Models\Users::where('status','!=','trashed')->where('user_type','=','tender')->get());

        DB::statement(DB::raw('set @id=0'));
        $users = \Models\Users::select(DB::raw('@id := 0 r'))
                    ->select(DB::raw('@id := @id + 1 AS ids'),'users.*')->where('status', '=', 'trashed')->where('user_type','tender');

        if ($request->ajax()) {
            return DataTables::of($users)
            ->editColumn('action',function($item){
                $html    = '<div class="edit_details_box">';
                $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/tender/status/?id=%s&status=active',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to restore '.$item['name'].' record?" title="Restore"><i class="fa fa-undo" aria-hidden="true"></i></a> | ';
                
                $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/tender/destroy/%s',___encrypt($item['id']))).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to permanently delete '.$item['name'].' record?" title="Permanent Delete"><i class="fa fa-trash"></i></a>';
                $html   .= '</div>';
                                
                return $html;
            })
            ->editColumn('status',function($item){
                return ucfirst($item['status']);
            })
             ->editColumn('name',function($item){
                return ucfirst($item['name']);
            })
            ->rawColumns(['action'])
            ->make(true);
        }

        $data['html'] = $builder
            ->parameters([
                "dom" => "<'row' <'col-md-6 col-sm-12 col-xs-4'l><'col-md-6 col-sm-12 col-xs-4'f>><'row filter'><'row white_box_wrapper database_table table-responsive'rt><'row' <'col-md-6'i><'col-md-6'p>>",
            ])
            ->addColumn(['data' => 'ids', 'name' => 'id','title' => 'S.No.','orderable' => true, 'width' => 40])
            ->addColumn(['data' => 'name', 'name' => 'name','title' => 'Name','orderable' => false, 'width' => 120])
            ->addColumn(['data' => 'email', 'name' => 'email','title' => 'Email','orderable' => false, 'width' => 120])
            ->addColumn(['data' => 'status','name' => 'status','title' => 'Status','orderable' => false, 'width' => 120])
            ->addAction(['title' => 'Action', 'orderable' => false, 'width' => 120]);
        return view('home')->with($data);
    }


    public function destroy($id)
    {
        $table_users = DB::table('users');
        $table_user = DB::table('tenders');
        $table_useraddress = DB::table('users_address');

        if (!empty($id)) 
        {
            $id = ___decrypt($id);

            $table_users->where('id','=',$id);
            $isDeleted = $table_users->delete(); 

            $table_user->where('user_id','=',$id);
            $table_user->delete(); 

            $table_useraddress->where('user_id','=',$id);
            $table_useraddress->delete(); 
        
            if($isDeleted)
            {
                $this->message = 'Tender deleted successfully.';
                $this->status = true;
                $this->redirect = url('tender/trash'); 
                $this->jsondata = [];
            }
        }
        return $this->populateresponse();
    }


}
