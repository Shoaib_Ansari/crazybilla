<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;
use Validations\Investor as Validations;
use Auth;
use DB;

class InvestorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(Request $request){
        parent::__construct($request);
        $this->middleware('auth');
    }


    public function index(Request $request, Builder $builder){
        $data['site_title'] = $data['page_title'] = 'Investor List';
        $data['menu']       = 'investor-list';
        $data['breadcrumb'] = '<ul class="page-breadcrumb breadcrumb"><li><a href="'.url('/').'"><i class="fa fa-home" style="font-size:12px;"></i> Home</a><i class="fa fa-circle"></i></li><li> &nbsp;<a href="">Investor</a></li></ul>';
        $data['view'] = 'admin.investor.list';
        //$users  = _arefy(\Models\Users::where('status','!=','trashed')->where('user_type','=','investor')->get());

       // dd(Auth::user()->id);
        if(Auth::user()->user_type == 'associate')
         { 
            DB::statement(DB::raw('set @id=0'));
            $users = \Models\Investor::select(DB::raw('@id := 0 r'))
                    ->select(DB::raw('@id := @id + 1 AS ids'),'investors.*')->where('status', '<>', 'trashed')->where('associate_id', Auth::user()->id);
         } 
         else
         { 

            DB::statement(DB::raw('set @id=0'));
            $users = \Models\Investor::select(DB::raw('@id := 0 r'))
                    ->select(DB::raw('@id := @id + 1 AS ids'),'investors.*')->where('status', '<>', 'trashed');
         }

        if ($request->ajax()) {
            return DataTables::of($users)
            ->editColumn('action',function($item){
                $item['id'] = $item['user_id'];
                $html    = '<div class="edit_details_box">';
                $html   .= '<a href="'.url(sprintf('admin/investor/%s',___encrypt($item['id']))).'" title="View Detail"><i class="fa fa-fw fa-eye"> </i></a> | ';
                $html   .= '<a href="'.url(sprintf('admin/investor/%s/edit',___encrypt($item['id']))).'"  title="Edit Detail"><i class="fa fa-edit"></i></a> | ';
                if($item['status'] == 'active'){
                    $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/investor/status/?id=%s&status=inactive',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/inactive-user.png').'"
                        data-ask="Would you like to change '.$item['name'].' status from active to inactive?" title="Update Status"><i class="fa fa-fw fa-ban"></i></a> | ';
                }elseif($item['status'] == 'inactive'){
                    $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/investor/status/?id=%s&status=active',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to change '.$item['name'].' status from inactive to active?" title="Update Status"><i class="fa fa-fw fa-check"></i></a> | ';
                }elseif($item['status'] == 'pending'){
                    $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/investor/status/?id=%s&status=active',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to change '.$item['name'].' status from pending to active?" title="Update Status"><i class="fa fa-fw fa-check"></i></a> | ';
                }
                $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/investor/status/?id=%s&status=trashed',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to delete '.$item['name'].' record?" title="Delete Status"><i class="fa fa-trash"></i></a>';
                $html   .= '</div>';
                                
                return $html;
            })
            ->editColumn('status',function($item){
                return ucfirst($item['status']);
            })
            ->editColumn('name',function($item){
                return ucfirst($item['name']);
            })
            ->editColumn('associate',function($item){
                return ucfirst($item['associate']['name']);
            })
            ->editColumn('assign', function($item) {
                $html = '<a href="#draggable" onclick="loadTender('.$item['id'].');" data-toggle="modal" class="btn btn-default btn-circle">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-480">
                        Assign Tenders </span>
                        </a>';
                return $html;
            })
            ->rawColumns(['action', 'assign'])
            ->make(true);
        }

        $data['html'] = $builder
            ->parameters([
                "dom" => "<'row' <'col-md-6 col-sm-12 col-xs-4'l><'col-md-6 col-sm-12 col-xs-4'f>><'row filter'><'row white_box_wrapper database_table table-responsive'rt><'row' <'col-md-6'i><'col-md-6'p>>",
            ])
            ->addColumn(['data' => 'ids', 'name' => 'id','title' => 'S.No.','orderable' => true, 'width' => 40])
            ->addColumn(['data' => 'name', 'name' => 'name','title' => 'Name','orderable' => false, 'width' => 100])
            ->addColumn(['data' => 'associate', 'name' => 'associate','title' => 'Associate','orderable' => false, 'width' => 100])
            ->addColumn(['data' => 'email', 'name' => 'email','title' => 'Email','orderable' => false, 'width' => 150])
            ->addColumn(['data' => 'status','name' => 'status','title' => 'Status','orderable' => false, 'width' => 90])
            ->addAction(['title' => 'Action', 'orderable' => false, 'width' => 120])
            ->addColumn(['data' => 'assign', 'name' => 'assign', 'title' => ' ', 'orderable' => false, 'width' => 100]);
        return view('home')->with($data);
    }
    public function create(Request $request){
        $data['site_title']       = $data['page_title'] = 'Create Investor';
        $data['menu']             = 'investor-add';
        $data['associateList']    = _arefy(\Models\Associate::where('status','=','active')->select('id', 'name')->get());
        $data['view']             = 'admin.investor.add';
        return view('home',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $validation = new Validations($request);
        $strict = '';
        if (Auth::user()->user_type == 'admin') {
            $strict = 'admin';
        }
        $validator  = $validation->createInvestor($strict);
        if($validator->fails()){
            $this->message = $validator->errors();
        }else{
            $data['name']               = $request->name;
            $data['full_name']          = $request->name;
            $data['email']              = $request->email;
            $data['mobile']             = $request->phone;
            $data['country_code']       = '+91';

            if ($request->profile_picture) {
                $image = $request->file('profile_picture');
                $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('images/investor');
                $image->move($destinationPath, $input['imagename']);
                $data['profile_picture'] = $input['imagename'];
            }

            $data['password']           = \Hash::make($request->password);
            $data['aadhar_no']          = $request->aadhar_no;
            $data['pan_no']             = $request->pan_no;
            $data['status']             = 'active';
            $data['user_type']          = 'investor';
            $data['otp']                = strtoupper(__random_string(6));
            $data['updated_at']         = date('Y-m-d H:i:s');
            $data['created_at']         = date('Y-m-d H:i:s');
            $inserId = \Models\Users::add($data);
            if($inserId){
                $investor['name']               = $request->name;
                $investor['father_name']        = $request->father_name;
                $investor['email']              = $request->email;
                $investor['phone']              = $request->phone;
                $investor['alternate_phone']    = ($request->alternate_phone == '') ? '0' : $request->alternate_phone;
                $investor['gender']             = $request->gender;
                $investor['user_id']            = $inserId;
                $investor['associate_id']       = $request->associate_id;
                $investor['invested_amount']    = $request->invested_amount;
                $investor['invest_date']        = date('Y-m-d H:i:s');
                $investor['current_balance']    = '0';
                $investor['status']             = 'active';
                $investor['updated_at']         = date('Y-m-d H:i:s');
                $investor['created_at']         = date('Y-m-d H:i:s');

                \Models\Investor::add($investor);

                $address_for_lat_long           = $request->city.' '.$request->state.' '.$request->country;
                $lat_long                       = _getGEOLocationByAddress($address_for_lat_long);

                $investor_address['user_id']    = $inserId;
                $investor_address['city']       = $request->city;
                $investor_address['state']      = $request->state;
                $investor_address['country']    = $request->country;
                $investor_address['pin_code']   = $request->pin_code;
                $investor_address['street']     = $request->street;
                $investor_address['latitude']   = $lat_long['lat'];         //'26.846695' for lucknow
                $investor_address['longitude']  = $lat_long['long'];        //'80.946167' for lucknow
                $investor_address['updated_at'] = date('Y-m-d H:i:s');
                $investor_address['created_at'] = date('Y-m-d H:i:s');

                \Models\UsersAddress::add($investor_address);

                $wallet['user_id']      = $inserId;
                $wallet['balance']      = 0;
                $wallet['status']       = 'verified';
                $wallet['last_updated'] = date('Y-m-d H:i:s');
                $wallet['created_at']   = date('Y-m-d H:i:s');
                $wallet['updated_at']   = date('Y-m-d H:i:s');

                \Models\Wallet::add($wallet);

            }
            $this->status   = true;
            $this->modal    = true;
            $this->alert    = true;
            $this->message  = "Investor has been added successfully.";
            $this->redirect = url('admin/investor');
        } 
        return $this->populateresponse();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['site_title'] = $data['page_title'] = 'View Investor';
        $data['menu']       = 'investor-view';
        $data['view'] = 'admin.investor.view';
        $id = ___decrypt($id);
        $data['investorDetails'] = _arefy(\Models\Investor::list('single',$id));
        $data['tenderDetails'] = _arefy(\Models\Tender::list('array', '', ' investor_id="'.$id.'"'));
        
        return view('home',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['site_title'] = $data['page_title'] = 'Edit Investor';
        $data['menu']       = 'investor-edit';
        $data['view'] = 'admin.investor.edit';
        $id = ___decrypt($id);
        $data['investorDetails'] = _arefy(\Models\Investor::list('single',$id));
        $data['associateList']    = _arefy(\Models\Associate::where('status','!=','trashed')->select('id', 'name')->get());
        return view('home',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id                     = ___decrypt($id);
        $request->id            = $id;
        $validation             = new Validations($request);

        $strict = '';
        if (Auth::user()->user_type == 'admin') {
            $strict = 'admin';
        }

        If($request->updateProfile)
        {
            $validator  = $validation->updateProfile('update');
        }
        else
        {
            $validator  = $validation->createInvestor($strict, 'edit');
        }

        if($validator->fails()){
            $this->message      = $validator->errors();
        }else{
            $data['name']       = $request->name;
            $data['full_name']  = $request->name;
            if ($request->email) 
            {
                $data['email']  = $request->email;
            }
            $data['mobile']     = $request->phone;
            if ($request->aadhar_no) 
            {
                $data['aadhar_no']  = $request->aadhar_no;
            }
            if ($request->pan_no) 
            {
                $data['pan_no']     = $request->pan_no;
            }
            

            if ($request->profile_picture) {
                $image = $request->file('profile_picture');
                $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('images/investor');
                $image->move($destinationPath, $input['imagename']);
                $data['profile_picture'] = $input['imagename'];
            }

            if ($request->password) {
                $data['password']           = \Hash::make($request->password);
            }
            $data['updated_at']             = date('Y-m-d H:i:s');
            $updatedInUser                  = \Models\Users::change($id, $data);

            $investor['name']               = $request->name;
            if ($request->father_name) 
            {
                $investor['father_name']    = $request->father_name;
            }
            
            if ($request->email) 
            {
                $investor['email']          = $request->email;
            }
            
            $investor['phone']              = $request->phone;
            if ($request->alternate_phone) 
            {
                $investor['alternate_phone']  = ($request->alternate_phone == '') ? '0' : $request->alternate_phone;
            }
            
            $investor['gender']             = $request->gender;
            if ($request->associate_id) 
            {
                $investor['associate_id']   = $request->associate_id;
            }
             if ($request->invested_amount) 
            {
                $investor['invested_amount']   = $request->invested_amount;
            }
            $investor['updated_at']         = date('Y-m-d H:i:s');

            $updateInInvestor               = \Models\Investor::change($id, $investor);

            $address_for_lat_long           = $request->city.' '.$request->state.' '.$request->country;
            $lat_long                       = _getGEOLocationByAddress($address_for_lat_long);

            $investor_address['city']       = $request->city;
            $investor_address['state']      = $request->state;
            $investor_address['country']    = $request->country;
            $investor_address['pin_code']   = $request->pin_code;
            $investor_address['street']     = $request->street;
            $investor_address['latitude']   = $lat_long['lat'];         //'26.846695' for lucknow
            $investor_address['longitude']  = $lat_long['long'];        //'80.946167' for lucknow
            $investor_address['updated_at'] = date('Y-m-d H:i:s');

            $updateInAddress                = \Models\UsersAddress::change($id, $investor_address);


            //}
            if($updatedInUser && $updateInInvestor && $updateInAddress) { 
                $this->status               = true;
                $this->modal                = true;
                $this->alert                = true;
                If($request->updateProfile)
                {
                    $this->message              = "Profile has been updated successfully.";
                    $this->redirect             = url('investor/profile');
                }
                else
                {
                    $this->message              = "The Investor has been updated successfully.";
                    $this->redirect             = url('admin/investor');   
                }
                
            } else {
                $this->status               = true;
                $this->modal                = true;
                $this->alert                = true;
                $this->message              = "There is some problem in the server right now, please try again after some time.";
            }
        }
        return $this->populateresponse();
    }

    
    public function changeStatus(Request $request)
    {
        $validation = new Validations($request);
        $validator = $validation->changeStatus();

        if($validator->fails()){
            $this->message = $validator->errors();
        }else{
            $userData                = ['status' => $request->status, 'updated_at' => date('Y-m-d H:i:s')];
            $isUpdate                = \Models\Users::change($request->id,$userData);
            \Models\Investor::change($request->id,$userData);

            if($isUpdate){
                if($request->status == 'trashed'){
                    $this->message = 'Deleted Investor successfully.';
                }else{
                    $this->message = 'Updated Investor successfully.';
                }
                $this->status       = true;
                $this->redirect     = true;
                $this->jsondata     = [];
            }
        }
        return $this->populateresponse();
    }


    public function profile(Request $request) {
        $data['site_title'] = $data['page_title'] = 'Edit Profile';
        $data['profileId'] = \Auth::user()->id;
        $data['menu']       = '-';
        $id = \Auth::user()->id;
        $data['UserDetails'] = _arefy(\Models\Users::where('id',$id)->select('*')->get());
        $data['profileDetails'] = _arefy(\Models\Investor::where('user_id',$id)->select('*')->get());
        $data['addressDetails'] = _arefy(\Models\UsersAddress::where('user_id',$id)->select('*')->get());
        $data['view'] = 'admin.investor.profile';
        return view('home',$data);
    }


    public function getTendersForInvestor(Request $request) {
        if ($request->investor_id) {
            $investor_id    = $request->investor_id;
            $tenders        = _arefy(\Models\Tender::list('array', NULL, ' investor_id="'.$investor_id.'" OR investor_id is NULL'));
            $this->status           = true;
            $this->redirect         = true;
            $this->jsondata         = $tenders;
            $this->message          = 'Tenders data found';
        } else {
            $this->status           = true;
            $this->redirect         = true;
            $this->jsondata         = [];
            $this->message          = 'Please provide investor id.';
        }
        return $this->populateresponse();
    }


    public function assignTender(Request $request) {
        $data['investor_id'] = $request->investor_id;
        \Models\Tender::where('investor_id', $data['investor_id'])->update(['investor_id' => NULL]);
        if (!empty($request->tender_ids)) {
            foreach($request->tender_ids as $tender_id) {
                $assignTender = \Models\Tender::change($tender_id, $data);
            }
        }
        if($assignTender) {
            $this->status           = true;
            $this->redirect         = true;
            $this->modal            = true;
            $this->jsondata         = [];
            $this->message          = 'Tenders successfully assigned';    
        }
        return $this->populateresponse();
    }


     public function trash(Request $request, Builder $builder){
        $data['site_title'] = $data['page_title'] = 'Trash Investor List';
        $data['menu']       = 'trash-investor';
        $data['breadcrumb'] = '<ul class="page-breadcrumb breadcrumb"><li><a href="'.url('/').'"><i class="fa fa-home" style="font-size:12px;"></i> Home</a><i class="fa fa-circle"></i></li><li> &nbsp;<a href="">Trash Investor</a></li></ul>';
        $data['view'] = 'admin.trash.investorlist';
       
            DB::statement(DB::raw('set @id=0'));
            $users = \Models\Investor::select(DB::raw('@id := 0 r'))
                    ->select(DB::raw('@id := @id + 1 AS ids'),'investors.*')->where('status', '=', 'trashed');
       
        if ($request->ajax()) {
            return DataTables::of($users)
            ->editColumn('action',function($item){
                $item['id'] = $item['user_id'];
                $html    = '<div class="edit_details_box">';
                
                $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/investor/status/?id=%s&status=active',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to restore '.$item['name'].' record?" title="Restore"><i class="fa fa-undo" aria-hidden="true"></i></a> | ';
                
                $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/investor/destroy/%s',___encrypt($item['id']))).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to permanently delete '.$item['name'].' record?" title="Permanent Delete"><i class="fa fa-trash"></i></a>';
                $html   .= '</div>';
                                
                return $html;
            })
            ->editColumn('status',function($item){
                return ucfirst($item['status']);
            })
             ->editColumn('name',function($item){
                return ucfirst($item['name']);
            })
            ->make(true);
        }

        $data['html'] = $builder
            ->parameters([
                "dom" => "<'row' <'col-md-6 col-sm-12 col-xs-4'l><'col-md-6 col-sm-12 col-xs-4'f>><'row filter'><'row white_box_wrapper database_table table-responsive'rt><'row' <'col-md-6'i><'col-md-6'p>>",
            ])
            ->addColumn(['data' => 'ids', 'name' => 'id','title' => 'S.No.','orderable' => true, 'width' => 40])
            ->addColumn(['data' => 'name', 'name' => 'name','title' => 'Name','orderable' => false, 'width' => 100])
            ->addColumn(['data' => 'email', 'name' => 'email','title' => 'Email','orderable' => false, 'width' => 150])
            ->addColumn(['data' => 'status','name' => 'status','title' => 'Status','orderable' => false, 'width' => 90])
            ->addAction(['title' => 'Action', 'orderable' => false, 'width' => 120]);
            
        return view('home')->with($data);
    }


    public function destroy($id)
    {
        $table_users = DB::table('users');
        $table_user = DB::table('investors');
        $table_useraddress = DB::table('users_address');

        if (!empty($id)) 
        {
            $id = ___decrypt($id);

            $table_users->where('id','=',$id);
            $isDeleted = $table_users->delete(); 

            $table_user->where('user_id','=',$id);
            $table_user->delete(); 

            $table_useraddress->where('user_id','=',$id);
            $table_useraddress->delete(); 
        
            if($isDeleted)
            {
                $this->message = 'Investor deleted successfully.';
                $this->status = true;
                $this->redirect = url('investor/trash'); 
                $this->jsondata = [];
            }
        }
        return $this->populateresponse();
    }
}
