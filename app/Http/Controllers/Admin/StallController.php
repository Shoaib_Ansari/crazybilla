<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;
use Validations\Stall as Validations;
use Image;
use Auth;
use DB;

class StallController extends Controller
{
    
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(Request $request){
        parent::__construct($request);
        $this->middleware('auth');
    }

    public function index(Request $request, Builder $builder)
    {
    	$data['site_title'] = $data['page_title'] = 'Stall List';
        $data['menu']       = 'stall-list';
        $data['breadcrumb'] = '<ul class="page-breadcrumb breadcrumb"><li><a href="'.url('/').'"><i class="fa fa-home" style="font-size:12px;"></i> Home</a><i class="fa fa-circle"></i></li><li> &nbsp;<a href="">Stall</a></li></ul>';
        $data['view'] = 'admin.stall.list';
       // $users  = _arefy(\Models\Users::where('status','!=','trashed')->where('user_type','=','stall')->get());

        DB::statement(DB::raw('set @id=0'));
        $users = \Models\Stall::select(DB::raw('@id := 0 r'))
                    ->select(DB::raw('@id := @id + 1 AS ids'),'stalls.*')->where('status', '<>', 'trashed');

        if ($request->ajax()) {
            return DataTables::of($users)
            ->editColumn('action',function($item){
                $html    = '<div class="edit_details_box">';
                $html   .= '<a href="'.url(sprintf('admin/stall/%s',___encrypt($item['user_id']))).'" title="View Detail"><i class="fa fa-fw fa-eye"></i></a> | ';
                $html   .= '<a href="'.url(sprintf('admin/stall/%s/edit',___encrypt($item['user_id']))).'"  title="Edit Detail"><i class="fa fa-edit"></i></a> | ';
                if($item['status'] == 'active'){
                    $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/stall/status/?id=%s&status=inactive',$item['user_id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/inactive-user.png').'"
                        data-ask="Would you like to change '.$item['name'].' Status from active to inactive?" title="Update Status"><i class="fa fa-fw fa-ban"></i></a> | ';
                }elseif($item['status'] == 'inactive'){
                    $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/stall/status/?id=%s&status=active',$item['user_id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to change '.$item['name'].' Status from inactive to active?" title="Update Status"><i class="fa fa-fw fa-check"></i></a> | ';
                }elseif($item['status'] == 'pending'){
                    $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/stall/status/?id=%s&status=active',$item['user_id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to change '.$item['name'].' Status from pending to active?" title="Update Status"><i class="fa fa-fw fa-check"></i></a> | ';
                }
                $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/stall/status/?id=%s&status=trashed',$item['user_id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to delete '.$item['name'].' record?" title="Delete Status"><i class="fa fa-trash"></i></a>';
                $html   .= '</div>';
                                
                return $html;
            })
            ->editColumn('status',function($item){
                return ucfirst($item['status']);
            })
             ->editColumn('name',function($item){
                return ucfirst($item['name']);
            })
            ->editColumn('assign', function($item) {
                $html = '<a href="'.url(sprintf('stall/assign/%s', ___encrypt($item['id']))).'" data-toggle="modal" class="btn btn-default btn-circle">
                        <i class="fa fa-cutlery"></i>
                        <span class="hidden-480">
                        Assign Products </span>
                        </a>';
                return $html;
            })
            ->rawColumns(['action', 'assign'])
            ->make(true);
        }

        $data['html'] = $builder
            ->parameters([
                "dom" => "<'row' <'col-md-6 col-sm-12 col-xs-4'l><'col-md-6 col-sm-12 col-xs-4'f>><'row filter'><'row white_box_wrapper database_table table-responsive'rt><'row' <'col-md-6'i><'col-md-6'p>>",
            ])
            ->addColumn(['data' => 'ids', 'name' => 'id','title' => 'S.No.','orderable' => true, 'width' => 40])
            ->addColumn(['data' => 'name', 'name' => 'name','title' => 'Name','orderable' => false, 'width' => 120])
            ->addColumn(['data' => 'email', 'name' => 'email','title' => 'Email','orderable' => false, 'width' => 120])
            ->addColumn(['data' => 'status','name' => 'status','title' => 'Status','orderable' => false, 'width' => 120])
            ->addAction(['title' => 'Actions', 'orderable' => false, 'width' => 120])
            ->addColumn(['data' => 'assign', 'name' => 'assign', 'title' => ' ', 'orderable' => false, 'width' => 100]);
        return view('home')->with($data);
    }


    public function create(Request $request){
        $data['site_title'] = $data['page_title'] = 'Create Stall';
        $data['menu']       = 'stall-add';
        $data['tenderList'] = _arefy(\Models\Tender::get());
        $data['view']       = 'admin.stall.add';
        return view('home',$data);
    }


    public function store(Request $request){
        $validation = new Validations($request);
        $validator  = $validation->createStall();
        if($validator->fails()){
            $this->message = $validator->errors();
        }else{
            $data['name']               = $request->name;
            $data['full_name']          = $request->name;
            $data['email']              = $request->email;
            $data['mobile']             = $request->mobile;
            $data['country_code']       = '+91';

            if ($request->profile_picture) {
                $image = $request->file('profile_picture');
                $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('images/stall');
                $image->move($destinationPath, $input['imagename']);
                $data['profile_picture'] = $input['imagename'];
            }

            $data['password']           = \Hash::make($request->password);
            $data['aadhar_no']          = $request->aadhar_no;
            $data['pan_no']             = $request->pan_no;
            $data['status']             = 'active';
            $data['user_type']          = 'stall';
            $data['otp']                = strtoupper(__random_string(6));
            $data['updated_at']         = date('Y-m-d H:i:s');
            $data['created_at']         = date('Y-m-d H:i:s');
            $inserId = \Models\Users::add($data);
            if($inserId){
                $stall['name']               = $request->name;
                $stall['user_id']            = $inserId;
                $stall['phone']    			 = $request->mobile;
                $stall['email']              = $request->email;
                $stall['alternate_phone']    = $request->alternate_phone;
                $stall['tender_id']          = $request->tender;
                $stall['status']             = 'active';
                $stall['updated_at']         = date('Y-m-d H:i:s');
                $stall['created_at']         = date('Y-m-d H:i:s');

                \Models\Stall::add($stall);

                $address_for_lat_long         = $request->city.' '.$request->state.' '.$request->country;
                $lat_long                     = _getGEOLocationByAddress($address_for_lat_long);

                $stall_address['user_id']    = $inserId;
                $stall_address['city']       = $request->city;
                $stall_address['state']      = $request->state;
                $stall_address['country']    = $request->country;
                $stall_address['pin_code']   = $request->pin_code;
                $stall_address['street']     = $request->street;
                $stall_address['latitude']   = $lat_long['lat'];         //'26.846695' for lucknow
                $stall_address['longitude']  = $lat_long['long'];        //'80.946167' for lucknow
                $stall_address['updated_at'] = date('Y-m-d H:i:s');
                $stall_address['created_at'] = date('Y-m-d H:i:s');

                \Models\UsersAddress::add($stall_address);
            }
            $this->status   = true;
            $this->modal    = true;
            $this->alert    = true;
            $this->message  = "Stall has been added successfully.";
            $this->redirect = url('admin/stall');
        } 
        return $this->populateresponse();
    }


    public function edit($id)
    {
        $data['site_title'] = $data['page_title'] = 'Edit Stall';
        $data['menu']       = 'stall-edit';
        $data['tenderList'] = _arefy(\Models\Tender::get());
        $data['view'] = 'admin.stall.edit';
        $id = ___decrypt($id);
        $data['stallDetails'] = _arefy(\Models\Stall::list('single',$id));

        return view('home',$data);
    }


     public function update(Request $request, $id){
        $id = ___decrypt($id);
        $request->id = $id;
        $validation = new Validations($request);
        $validator  = $validation->createStall('edit');
        if($validator->fails()){
            $this->message = $validator->errors();
        }else{
            $data['name']               = $request->name;
            $data['full_name']          = $request->name;
            $data['email']              = $request->email;
            $data['mobile']             = $request->mobile;
            $data['country_code']       = '+91';

           	if ($request->profile_picture) {
                $image = $request->file('profile_picture');
                $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('images/stall');
                $image->move($destinationPath, $input['imagename']);
                $data['profile_picture'] = $input['imagename'];
            }
            else
            {
              $data['profile_picture'] = $request->old_profile_picture;  
            }

            $data['aadhar_no']          = $request->aadhar_no;
            $data['pan_no']             = $request->pan_no;
            $data['status']             = 'active';
            $data['user_type']          = 'stall';
            $data['otp']                = strtoupper(__random_string(6));
            $data['updated_at']         = date('Y-m-d H:i:s');
            \Models\Users::change($id, $data);
                $stall['name']               = $request->name;
                $stall['phone']    			 = $request->mobile;
                $stall['email']              = $request->email;
                $stall['alternate_phone']    = $request->alternate_phone;
                $stall['tender_id']          = $request->tender;
                $stall['status']             = 'active';
                $stall['updated_at']         = date('Y-m-d H:i:s');

                \Models\Stall::change($id, $stall);

                $address_for_lat_long         = $request->city.' '.$request->state.' '.$request->country;
                $lat_long                     = _getGEOLocationByAddress($address_for_lat_long);

                $stall_address['city']       = $request->city;
                $stall_address['state']      = $request->state;
                $stall_address['country']    = $request->country;
                $stall_address['pin_code']   = $request->pin_code;
                $stall_address['street']     = $request->street;
                $stall_address['latitude']   = $lat_long['lat'];         //'26.846695' for lucknow
                $stall_address['longitude']  = $lat_long['long'];        //'80.946167' for lucknow
                $stall_address['updated_at'] = date('Y-m-d H:i:s');

                \Models\UsersAddress::change($id, $stall_address);

            $this->status   = true;
            $this->modal    = true;
            $this->alert    = true;
            $this->message  = "Stall has been updated successfully.";
            $this->redirect = url('admin/stall');
        } 
        return $this->populateresponse();
    }



    public function changeStatus(Request $request)
    {
        $validation = new Validations($request);
        $validator = $validation->changeStatus();

        if($validator->fails()){
            $this->message = $validator->errors();
        }else{
            $userData = ['status' => $request->status, 'updated_at' => date('Y-m-d H:i:s')];
            $isUpdate = \Models\Users::change($request->id,$userData);
            \Models\Stall::change($request->id,$userData);

            if($isUpdate){
                if($request->status == 'trashed'){
                    $this->message = 'Deleted Associate successfully.';
                }else{
                    $this->message = 'Updated Associate successfully.';
                }
                $this->status = true;
                $this->redirect = true;
                $this->jsondata = [];
            }
        }
        return $this->populateresponse();
    }



    public function assignProducts($id) {
        $id                             = ___decrypt($id);
        $data['site_title']             = $data['page_title'] = 'Assign products';
        $data['menu']                   = 'products-assign';
        $data['productList']            = _arefy(\Models\Product::where('status', 'active')->get());
        $data['stallDetails']           = _arefy(\Models\Stall::where('id',$id)->first());
        $data['stallsProducts']         = _arefy(\Models\product_to_stall::list('array',$id));
        $data['assignedProducts']       = [];
        foreach ($data['stallsProducts'] as $products) {
            $data['assignedProducts'][] = $products['product_id'];
        }
        // dd($data['stallsProducts']);
        $data['view']                   = 'admin.stall.assignproduct';
        return view('home',$data);
    }



    public function saveproduct(Request $request) {
        $data['stall_id']           = $request->stall_id;
        $data['product_id']         = $request->product_id;
        $data['quantity']           = 0;
        $save = \Models\product_to_stall::add($data);
        if ($save) {
            $stallsProducts         = \Models\product_to_stall::list('single', $data['stall_id'], 'id = '.$save);
            $this->message = 'Product assigned successfully.';
            $this->status = true;
            $this->redirect = true;
            $this->jsondata = (object)$stallsProducts;
        }
        return $this->populateresponse();
    }



    public function removeproduct(Request $request) {
        $stall_id               = $request->stall_id;
        $product_id             = $request->product_id;
        $existing_quantity      = _arefy(\Models\Product::where('id', '=', $product_id)->select('quantity')->first())['quantity'];
        $stall_quantity         = _arefy(\Models\product_to_stall::where('stall_id', $stall_id)->where('product_id', $product_id)->select('quantity')->first())['quantity'];
        $product['quantity']    = $existing_quantity + $stall_quantity;
        $save = \Models\product_to_stall::where('stall_id', $stall_id)->where('product_id', $product_id)->delete();
        if ($save) {
            \Models\Product::where('id', $product_id)->update($product);
            $this->message = 'Product removed successfully.';
            $this->status = true;
            $this->redirect = true;
            $this->jsondata = [];
        }
        return $this->populateresponse();
    }



    public function changequantity(Request $request) {
        $stall_id               = $request->stall_id;
        $product_id             = $request->product_id;
        $data['quantity']       = $request->quantity;
        $existing_quantity      = _arefy(\Models\Product::where('id', '=', $product_id)->select('quantity')->first())['quantity'];
        $stall_quantity         = _arefy(\Models\product_to_stall::where('product_id', '=', $product_id)->where('stall_id', '=', $stall_id)->select('quantity')->first())['quantity'];
        $product['quantity']    = $existing_quantity - ($data['quantity'] -$stall_quantity);
        $save = \Models\product_to_stall::where('stall_id', $stall_id)->where('product_id', $product_id)->update($data);
        if ($save) {
            \Models\Product::where('id', $product_id)->update($product);
            $this->message      = 'Quantity updated successfully.';
            $this->status       = true;
            $this->redirect     = true;
            $this->jsondata     = [];
        }
        return $this->populateresponse();
    }


    public function checkquantity(Request $request) {
        $product_id             = $request->product_id;
        $entered_quantity       = $request->quantity;
        $available_quantity     = _arefy(\Models\Product::where('id', '=', $product_id)->select('quantity')->first())['quantity'];
        if ($entered_quantity > $available_quantity) {
            $this->message      = 'Quantity exceeded.';
            $this->status       = true;
            $this->redirect     = true;
            $this->jsondata     = ['available' => 0];
        } else {
            $this->message      = 'Quantity available.';
            $this->status       = true;
            $this->redirect     = true;
            $this->jsondata     = ['available' => 1];
        }
        return $this->populateresponse();
    }




    public function trash(Request $request, Builder $builder)
    {
        $data['site_title'] = $data['page_title'] = 'Trash Stall List';
        $data['menu']       = 'trash-stall';
        $data['breadcrumb'] = '<ul class="page-breadcrumb breadcrumb"><li><a href="'.url('/').'"><i class="fa fa-home" style="font-size:12px;"></i> Home</a><i class="fa fa-circle"></i></li><li> &nbsp;<a href="">Trash Stall</a></li></ul>';
        $data['view'] = 'admin.trash.stall_list';
       // $users  = _arefy(\Models\Users::where('status','!=','trashed')->where('user_type','=','stall')->get());

        DB::statement(DB::raw('set @id=0'));
        $users = \Models\Users::select(DB::raw('@id := 0 r'))
                    ->select(DB::raw('@id := @id + 1 AS ids'),'users.*')->where('user_type','=','stall')->where('status', '=', 'trashed');

        if ($request->ajax()) {
            return DataTables::of($users)
            ->editColumn('action',function($item){
                $html    = '<div class="edit_details_box">';
                $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/stall/status/?id=%s&status=active',$item['id'])).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to restore '.$item['name'].' record?" title="Restore"><i class="fa fa-undo" aria-hidden="true"></i></a> | ';
                
                $html   .= '<a href="javascript:void(0);" 
                        data-url="'.url(sprintf('admin/stall/destroy/%s',___encrypt($item['id']))).'" 
                        data-request="ajax-confirm"
                        data-ask_image="'.url('/images/active-user.png').'"
                        data-ask="Would you like to permanently delete '.$item['name'].' record?" title="Permanent Delete"><i class="fa fa-trash"></i></a>';
                $html   .= '</div>';
                                
                return $html;
            })
            ->editColumn('status',function($item){
                return ucfirst($item['status']);
            })
             ->editColumn('name',function($item){
                return ucfirst($item['name']);
            })
            ->rawColumns(['action'])
            ->make(true);
        }

        $data['html'] = $builder
            ->parameters([
                "dom" => "<'row' <'col-md-6 col-sm-12 col-xs-4'l><'col-md-6 col-sm-12 col-xs-4'f>><'row filter'><'row white_box_wrapper database_table table-responsive'rt><'row' <'col-md-6'i><'col-md-6'p>>",
            ])
            ->addColumn(['data' => 'ids', 'name' => 'id','title' => 'S.No.','orderable' => true, 'width' => 40])
            ->addColumn(['data' => 'name', 'name' => 'name','title' => 'Name','orderable' => false, 'width' => 120])
            ->addColumn(['data' => 'email', 'name' => 'email','title' => 'Email','orderable' => false, 'width' => 120])
            ->addColumn(['data' => 'status','name' => 'status','title' => 'Status','orderable' => false, 'width' => 120])
            ->addAction(['title' => 'Action', 'orderable' => false, 'width' => 120]);
        return view('home')->with($data);
    }


    public function destroy($id)
    {
        $table_users = DB::table('users');
        $table_user = DB::table('stalls');
        $table_useraddress = DB::table('users_address');

        if (!empty($id)) 
        {
            $id = ___decrypt($id);

            $table_users->where('id','=',$id);
            $isDeleted = $table_users->delete(); 

            $table_user->where('user_id','=',$id);
            $table_user->delete(); 

            $table_useraddress->where('user_id','=',$id);
            $table_useraddress->delete(); 
        
            if($isDeleted)
            {
                $this->message = 'Stall deleted successfully.';
                $this->status = true;
                $this->redirect = url('stall/trash'); 
                $this->jsondata = [];
            }
        }
        return $this->populateresponse();
    }
}
