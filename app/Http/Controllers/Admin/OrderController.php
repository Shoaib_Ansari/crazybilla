<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;
use Image;
use DB;
use App\Quotation;

class OrderController extends Controller
{
    public function __construct(Request $request){
        parent::__construct($request);
        $this->middleware('auth');
    }


    public function index(Request $request, Builder $builder)
    {
    	$data['site_title'] = $data['page_title'] = 'Order List';
        $data['menu']       = 'order-list';
        $data['breadcrumb'] = '<ul class="page-breadcrumb breadcrumb"><li><a href="'.url('/').'"><i class="fa fa-home" style="font-size:12px;"></i> Home</a><i class="fa fa-circle"></i></li><li> &nbsp;<a href="">Order</a></li></ul>';
        $data['view'] = 'admin.order.list';
        
        DB::statement(DB::raw('set @id=0'));
        
        $users = \Models\Order::select(DB::raw('@id := 0 r'))
                    ->select(DB::raw('@id := @id + 1 AS ids'),'orders.*')
                    ->with(['stall' => function($q){
                        $q->select('*');
                    }])->get();
           // dd(json_decode(json_encode($users)));        
        if ($request->ajax()) {
            return DataTables::of($users)
            ->editColumn('action',function($item){
                $html    = '<div class="edit_details_box">';
                
                $html   .= '</div>';
                                
                return $html;
            })
             ->editColumn('name',function($item){
                return ucfirst($item['stall']['name']);
            })
             ->editColumn('status',function($item){
                return ucfirst($item['status']);
            })
           
            ->make(true);

        }

        $data['html'] = $builder
            ->parameters([
                "dom" => "<'row' <'col-md-6 col-sm-12 col-xs-4'l><'col-md-6 col-sm-12 col-xs-4'f>><'row filter'><'row white_box_wrapper database_table table-responsive'rt><'row' <'col-md-6'i><'col-md-6'p>>",
            ])
            ->addColumn(['data' => 'ids', 'name' => 'id','title' => 'S.No.','orderable' => true, 'width' => 20])
            ->addColumn(['data' => 'order_id', 'name' => 'order_id','title' => 'OrderId','orderable' => true, 'width' => 20])
            ->addColumn(['data' => 'name', 'name' => 'name','title' => 'Stall','orderable' => true, 'width' => 120])
            ->addColumn(['data' => 'total_price', 'name' => 'total_price','title' => 'Total Price','orderable' => true, 'width' => 120])
            ->addColumn(['data' => 'status','name' => 'status','title' => 'Status','orderable' => true, 'width' => 60]);
        return view('home')->with($data);
    }
}
