<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use Validator;

class LoginController extends Controller
{
    public function authentication(Request $request)
    {
    	$validator = $request -> validate([
	            'email' 	=> 'required|email',
	            'password'  => 'required'
	        ],
	        [
			    'email.required' => 'Please enter :attribute .',
			    'email.email'    => 'Please enter valid :attribute.',
			]
    	);


    	if ($validator) 
    		{
	    	if (Auth::attempt(['email'=>$request->email,'password'=>$request->password])) 
	    		{
		    		return redirect('home');
		    	}
		    	else
		    	{
		    		//return redirect('login');
		    		Session::flash('success_msg', '<div class="alert alert-danger"><strong>Danger!</strong> Invalid Username Or Password!</div>');
	    			return redirect('login');
		    	}
		    }
		    else
		    {
   			return redirect('login')->withErrors($validator)->withInput();
   		}

    }


    public function logout()
    {	
    	Auth::logout();
    	return redirect('login');
    }
}
