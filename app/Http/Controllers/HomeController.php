<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['site_title'] = $data['page_title'] = 'Home';
        $data['menu']       = 'dashboard-';
        $data['breadcrumb'] = '<ul class="page-breadcrumb breadcrumb"><li><a href="">Home</a><i class="fa fa-circle"></i></li></ul>';
        switch (Auth::user()->user_type) {
            case 'admin':
                $data['totalAssociates']    = \Models\Associate::where('status', '!=', 'trashed')->count();
                $data['totalInvestors']     = \Models\Investor::where('status', '!=', 'trashed')->count();
                $data['totalTenders']       = \Models\Tender::where('status', '!=', 'trashed')->count();
                $data['view']               = 'home_admin';
                break;

            case 'associate':
                $data['totalInvestors']     = \Models\Investor::where('status', '!=', 'trashed')->where('associate_id', Auth::user()->id)->count();
                $data['view']               = 'home_associate'; 
                break;

            case 'investor':
                $data['totalTenders']       = \Models\Tender::where('status', '!=', 'trashed')->count();
                $data['totalStalls']        = \Models\Stall::where('status', '!=', 'trashed')->count();
                $data['view']               = 'home_investor';
                break;

            case 'tender':
                $data['totalAssociates']    = \Models\Associate::where('status', '!=', 'trashed')->count();
                $data['totalInvestors']     = \Models\Investor::where('status', '!=', 'trashed')->count();
                $data['view']               = 'home_investor';
                break;

            case 'investor':
                $data['totalAssociates']    = \Models\Associate::where('status', '!=', 'trashed')->count();
                $data['totalInvestors']     = \Models\Investor::where('status', '!=', 'trashed')->count();
                $data['view']               = 'home_investor';
                break;
            
            default:
                return redirect('error_page');
                break;
        }
        
        return view('home',$data);
    }

    
}
