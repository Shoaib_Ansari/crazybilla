<?php

namespace Validations;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
/**
* 
*/
class Profile
{
	protected $data;
	public function __construct($data){
		$this->data = $data;
	}

	private function validation($key){
		$validation = [
			'id'				=> ['required'],
			'currentpassword' 	=> ['required','string'],
			'password' 			=> ['required','min:6','same:confirmpassword'],
			'confirmpassword' 	=> ['required','min:6'],
			'full_name' 		=> ['required','string'],
			'mobile' 			=> ['required','string'],
			'email' 			=> ['required','string']
			

		];
		return $validation[$key];
	}


	public function updatePassword($action='update')
	{

        $validations['currentpassword'] = $this->validation('currentpassword');
        $validations['password'] = $this->validation('password');
		$validations['confirmpassword'] = $this->validation('confirmpassword');
		
    	$validator = \Validator::make($this->data->all(), $validations,[]);
        
        return $validator;	
	}


	public function updateProfile($action='update')
	{ 
		$validations['full_name'] = $this->validation('full_name');
		$validations['mobile'] = array_merge($this->validation('mobile'),[Rule::unique('users')->ignore($this->data->id,'id') ]);	
		$validations['email'] = $this->validation('email');
		

		$validator = \Validator::make($this->data->all(), $validations,[]);
        
        return $validator;	
	}


}