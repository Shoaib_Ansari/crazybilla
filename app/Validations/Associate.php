<?php

namespace Validations;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
/**
* 
*/
class Associate
{
	protected $data;
	public function __construct($data){
		$this->data = $data;
	}

	private function validation($key){
		$validation = [
			'id'				=> ['required'],
			'email'				=> ['required','email'],
			'first_name' 		=> ['required','string'],
			'last_name' 		=> ['required','string'],
			'Associate_id' 		=> ['required','string'],
			'date_of_birth' 	=> ['required','string'],
			'gender' 			=> ['required','string'],
			'phone_code' 		=> ['required','string'],
			'mobile_number' 	=> ['required','numeric','digits:10'],
			'aadhar_no'			=> ['required','numeric','digits:12'],
			'pan_no'			=> ['required'],
			'address' 			=> ['required','string','max:500'],
			'marital_status' 	=> ['required','string'],
			'date_of_joining' 	=> ['required','string'],
			'profile_picture' 	=> ['required', 'mimes:jpeg,png,jpg,gif,svg'],
			'pin_code' 			=> ['required','max:6','min:4'],
			'password' 			=> ['required','min:6','same:confirm_password'],
			'confirm_password' 	=> ['required','min:6'],
			'category_name'     => ['required','string'],
			'category_id'       => ['required','numeric'],
			'subcategory_name'  => ['required','string']
		];
		return $validation[$key];
	}

	public function createAssociate($action='add'){
		// dd($this->data->id);s
        $validations = [
            'first_name' 		=> $this->validation('first_name'),
            'last_name' 		=> $this->validation('last_name'),
            'email' 			=> array_merge($this->validation('email'),[Rule::unique('users')->ignore('trashed','status')]),
			'country_code'		=> $this->validation('phone_code'),
			'mobile'  			=> array_merge($this->validation('mobile_number'),[Rule::unique('users')->ignore('trashed','status')]),
			'gender'			=> $this->validation('gender'),
            'date_of_birth' 	=> $this->validation('date_of_birth'),
            'aadhar_no'			=> $this->validation('aadhar_no'),
            'pan_no'			=> $this->validation('pan_no'),
			'street'			=> $this->validation('address'),
			'city'				=> $this->validation('address'),
			'state'				=> $this->validation('address'),
			'pin_code'			=> $this->validation('pin_code'),
			'country'			=> $this->validation('address')

    	];

    	if($action == 'edit'){
    		// $validations['id'] = $this->validation('id');
    		// dd($this->data->id);
			$validations['email'] = array_merge($this->validation('email'),[
				Rule::unique('users')->ignore($this->data->id,'id')
			]);
			$validations['mobile'] = array_merge($this->validation('mobile_number'),[
				Rule::unique('users')->ignore($this->data->id,'id') ]);	

		} else {
			$validations['password'] = $this->validation('password');
			$validations['confirm_password'] = $this->validation('confirm_password');
			$validations['profile_picture'] =  $this->validation('profile_picture');


		}
        $validator = \Validator::make($this->data->all(), $validations,[]);
        
        return $validator;		
	}

	public function createCategory($action='add')
	{
		$validations = [

			'category_name'  => array_merge($this->validation('category_name'),[Rule::unique('categories', 'name')])
		];

		 $validator = \Validator::make($this->data->all(), $validations,[]);
        
        return $validator;	
	}

	public function editCategory($action='edit')
	{
		$validations = [

			'category_name'  => array_merge($this->validation('category_name'),[Rule::unique('categories', 'name')->ignore($this->data->id,'id')])
		];

		 $validator = \Validator::make($this->data->all(), $validations,[]);
        
        return $validator;	
	}


	public function createSubcategory($action='add')
	{
		$validations = [

			'category_id'  => $this->validation('category_id'),
			'subcategory_name'  => array_merge($this->validation('subcategory_name'),[Rule::unique('subcategories', 'name')])
		];

		 $validator = \Validator::make($this->data->all(), $validations,[]);
        
        return $validator;	
	}

	public function editSubcategory($action='edit')
	{
		$validations = [

			'category_id'  => $this->validation('category_id'),
			'subcategory_name'  => array_merge($this->validation('subcategory_name'),[Rule::unique('subcategories', 'name')->ignore($this->data->id,'id')])	

		];

		 $validator = \Validator::make($this->data->all(), $validations,[]);
        
        return $validator;	
	}

	public function createAdmin($action='add'){
        $validations = [
            'first_name' 		=> $this->validation('first_name'),
            'last_name' 		=> $this->validation('last_name'),
			'email'				=> $this->validation('email'),
			'mobile_number'		=> $this->validation('mobile_number'),
            'date_of_birth' 	=> $this->validation('date_of_birth'),
			'gender'			=> $this->validation('gender'),
			/*'current_address'	=> $this->validation('address'),
			'permanent_address'	=> $this->validation('address'),*/
			'password' 			=> 'required|min:6',
			'password_confirmation' => 'required |same:password|min:6'
    	];

    	if($action == 'edit'){
    		$validations['id'] = $this->validation('id');
    	}

        $validator = \Validator::make($this->data->all(), $validations,[]);
        
        return $validator;		
	}


	public function changeStatus(){
		$validator = Validator::make($this->data->all(),[
			'id'    			=> 'required',
			'status'    		=> 'required',
		]);
		return $validator;
	}


	public function updateProfile($action='update'){
		// dd($this->data->id);s
        $validations = [
            'first_name' 		=> $this->validation('first_name'),
            'last_name' 		=> $this->validation('last_name'),
			'mobile'  	=> array_merge($this->validation('mobile_number'),[Rule::unique('users')->ignore($this->data->id,'id')]),
			'gender'			=> $this->validation('gender'),
            'date_of_birth' 	=> $this->validation('date_of_birth'),
			'street'			=> $this->validation('address'),
			'city'				=> $this->validation('address'),
			'state'				=> $this->validation('address'),
			'pin_code'			=> $this->validation('pin_code'),
			'country'			=> $this->validation('address')

    	];

        $validator = \Validator::make($this->data->all(), $validations,[]);
        return $validator;		
	}

}