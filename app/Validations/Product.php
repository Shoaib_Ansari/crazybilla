<?php

namespace Validations;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
/**
* 
*/
class Product
{
	protected $data;
	public function __construct($data){
		$this->data = $data;
	}

	private function validation($key){
		$validation = [
			'id'					=> ['required'],
			'name' 					=> ['required','string'],
			'category_id' 			=> ['required','string'],
			'productname' 			=> ['required','string'],
			'image' 				=> ['required', 'mimes:jpeg,png,jpg,gif,svg'],
			'image_edit' 			=> ['mimes:jpeg,png,jpg,gif,svg'],
			'description' 			=> ['required','string'],
			'price' 				=> ['required','string'],
			'discount_price' 		=> ['required','string'],
			'discount_percent' 		=> ['required','string'],
			'quantity' 				=> ['required','string'],
			'quantity_type' 		=> ['required','string'],


		];
		return $validation[$key];
	}

	public function createQtyType($action='add'){
        $validations['name'] 		= array_merge($this->validation('name'),[Rule::unique('product_quantity_types')->ignore('trashed','status')]);
    	if($action == 'edit'){
			$validations['name'] 	= array_merge($this->validation('name'),[
														Rule::unique('product_quantity_types')->ignore('trashed','status')->where(function($query){
															$query->where('id','!=',$this->data->id);
														})
													]
												);
		}
        $validator = \Validator::make($this->data->all(), $validations,[]);
        return $validator;		
	}


	public function createProduct($action='add')
	{
		$validations = [
            'category_id' 		=> $this->validation('category_id'),
            'productname' 		=> $this->validation('productname'),
			'image'				=> $this->validation('image'),
            'description' 		=> $this->validation('description'),
            'price'				=> $this->validation('price'),
            'discount_price'	=> $this->validation('discount_price'),
			'discount_percent'	=> $this->validation('discount_percent'),
			'quantity'			=> $this->validation('quantity'),
			'quantity_type'		=> $this->validation('quantity_type')
			
    	];

    	if ($action == 'edit') {
    		$validations['image'] = $this->validation('image_edit');
    	}

    	$validator = \Validator::make($this->data->all(), $validations,[]);
        
        return $validator;	
	}


	public function changeStatus(){
		$validator = Validator::make($this->data->all(),[
			'id'    			=> 'required',
			'status'    		=> 'required',
		]);
		return $validator;
	}	
}