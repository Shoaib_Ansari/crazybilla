<?php

namespace Validations;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
/**
* 
*/
class Stall
{
	protected $data;
	public function __construct($data){
		$this->data = $data;
	}

	private function validation($key){
		$validation = [
			'id'				=> ['required'],
			'email'				=> ['required','email'],
			'name' 				=> ['required','string'],
			'phone_code' 		=> ['required','string'],
			'number' 			=> ['required','numeric'],
			'mobile_number' 	=> ['required','numeric','digits:10'],
			'aadhar_no'			=> ['required','numeric', 'digits:12'],
			'pan_no'			=> ['required','alpha_num'],
			'address' 			=> ['required','string','max:500'],
			'profile_picture' 	=> ['required','mimes:jpeg,png,jpg,gif,svg'],
			'pin_code' 			=> ['required','digits:6'],
			'password' 			=> ['required','min:6','same:confirm_password'],
			'confirm_password' 	=> ['required','min:6'],
			'tender_id'			=> ['required']
		];
		return $validation[$key];
	}

	public function createStall($action='add'){
        $validations = [
        	'tender' 		    => $this->validation('tender_id'),
            'name' 				=> $this->validation('name'),
			'aadhar_no' 		=> $this->validation('aadhar_no'),
			'pan_no' 			=> $this->validation('pan_no'),
			'street'			=> $this->validation('address'),
			'city'				=> $this->validation('address'),
			'state'				=> $this->validation('address'),
			'pin_code'			=> $this->validation('pin_code'),
			'country'			=> $this->validation('address'),
    	];


    	
    	if($action == 'edit') { 
			$validations['email'] = array_merge($this->validation('email'),[
				Rule::unique('users')->ignore($this->data->id,'id')
			]);
			$validations['mobile'] = array_merge($this->validation('mobile_number'),[
				Rule::unique('users')->ignore($this->data->id,'id') ]);
		} else {
			$validations['email'] = array_merge($this->validation('email'),[Rule::unique('users')->ignore('trashed','status')]);
			$validations['mobile'] = array_merge($this->validation('mobile_number'),[Rule::unique('users')->ignore('trashed','status')]);
			$validations['password'] = $this->validation('password');
			$validations['confirm_password'] = $this->validation('confirm_password');
			$validations['profile_picture'] =  $this->validation('profile_picture');


		}
        $validator = \Validator::make($this->data->all(), $validations,[]);
        
        return $validator;		
	}


	public function changeStatus(){
		$validator = Validator::make($this->data->all(),[
			'id'    			=> 'required',
			'status'    		=> 'required',
		]);
		return $validator;
	}	
}