<?php

namespace Validations;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
/**
* 
*/
class Investor
{
	protected $data;
	public function __construct($data){
		$this->data = $data;
	}

	private function validation($key){
		$validation = [
			'id'						=> ['required'],
			'email'						=> ['required','email'],
			'name' 						=> ['required','string'],
			'Associate_id' 				=> ['required','numeric'],
			'amount' 					=> ['required','numeric'],
			'date_of_birth' 			=> ['required','string'],
			'gender' 					=> ['required','string'],
			'phone_code' 				=> ['required','string'],
			'mobile_number' 			=> ['required','numeric','digits:10'],
			'aadhar_no'					=> ['required','numeric','digits:12'],
			'pan_no'					=> ['required'],
			'address' 					=> ['required','string','max:500'],
			'marital_status' 			=> ['required','string'],
			'date_of_joining' 			=> ['required','string'],
			'profile_picture' 			=> ['required', 'mimes:jpeg,png,jpg,gif,svg'],
			'profile_picture_edit' 		=> ['mimes:jpeg,png,jpg,gif,svg'],
			'pin_code' 					=> ['required','digits:6'],
			'password' 					=> ['required','min:6','same:confirm_password'],
			'confirm_password' 			=> ['required','min:6'],
			'password_edit' 			=> ['same:confirm_password'],
			'confirm_password_edit' 	=> [],
		];
		return $validation[$key];
	}

	public function createInvestor($strict, $action='add'){
        $validations = [
            'name' 						=> $this->validation('name'),
			'email'  					=> array_merge($this->validation('email'),[Rule::unique('users')->ignore('trashed','status')]),
			'phone'  					=> $this->validation('mobile_number'),
			'gender'					=> $this->validation('gender'),
			'aadhar_no' 				=> $this->validation('aadhar_no'),
			'pan_no' 					=> $this->validation('pan_no'),
            'invested_amount' 			=> $this->validation('amount'),
			'street'					=> $this->validation('address'),
			'city'						=> $this->validation('address'),
			'state'						=> $this->validation('address'),
			'pin_code'					=> $this->validation('pin_code'),
			'country'					=> $this->validation('address'),
			'password'					=> $this->validation('password'),
			'confirm_password'			=> $this->validation('confirm_password'),
    	];
    	if ($strict == 'admin') {
    		$validations['associate_id'] = $this->validation('Associate_id');
    	}
    	if($action == 'edit'){
    		// $validations['id'] = $this->validation('id');
			$validations['email'] = array_merge($this->validation('email'),[
				Rule::unique('users')->ignore('trashed','status')->where(function($query){
					$query->where('id','!=',$this->data->id);
				})
			]);
			$validations['phone'] = array_merge($this->validation('mobile_number'),[
				Rule::unique('users', 'mobile')->ignore('trashed','status')->where(function($query){
					$query->where('id','!=',$this->data->id);
				})
			]);
			$validations['profile_picture'] = $this->validation('profile_picture_edit');
			$validations['password'] = $this->validation('password_edit');
			$validations['confirm_password'] = $this->validation('confirm_password_edit');
		}
        $validator = \Validator::make($this->data->all(), $validations,[]);
        
        return $validator;		
	}


	public function changeStatus(){
		$validator = Validator::make($this->data->all(),[
			'id'    			=> 'required',
			'status'    		=> 'required',
		]);
		return $validator;
	}


	public function updateProfile($action='update'){
        $validations = [
            'name' 		=> $this->validation('name'),
			'mobile_number'  	=> array_merge($this->validation('mobile_number'),[Rule::unique('users')->ignore($this->data->id,'id') ]),
			'gender'	=> $this->validation('gender'),
			'street'	=> $this->validation('address'),
			'city'		=> $this->validation('address'),
			'state'		=> $this->validation('address'),
			'pin_code'	=> $this->validation('pin_code'),
			'country'	=> $this->validation('address'),
    	];
    	
        $validator = \Validator::make($this->data->all(), $validations,[]);
        
        return $validator;		
	}	
}