<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Wallet extends Model
{
    protected $primaryKey = 'id';

     protected $fillable = [
        'user_id',
        'balance',
        'status',
        'last_updated',
        'created_at',     
        'updated_at'
    ];

    public static function add($data){
        if(!empty($data)){
            return self::insertGetId($data);
        }else{
            return false;
        }   
    }  



    public static function change($userID,$data){
        $isUpdated = false;
        $table_users = DB::table('subcategories');
        if(!empty($data)){
            $table_users->where('id','=',$userID);
            $isUpdated = $table_users->update($data); 
        }
                
        return (bool)$isUpdated;
    }
}
