<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product_to_stall extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'stall_id',
        'product_id',
        'quantity',
        'created_at',     
        'updated_at'
    ];


    public function stall(){
        return $this->hasOne('\Models\Stall','id','stall_id');
    }


    public function product(){
        return $this->hasOne('\Models\Product','id','product_id');
    }


    public static function add($data){
        if(!empty($data)){
            return self::insertGetId($data);
        }else{
            return false;
        }   
    }



    public static function change($userID,$data){
        $isUpdated = false;
        $table = DB::table('product_to_stalls');
        if(!empty($data)){
            $table->where('user_id','=',$userID);
            $isUpdated = $table->update($data); 
        }
                
        return (bool)$isUpdated;
    }


    public static function list($fetch='array',$stall_id=NULL,$where='',$order='id-desc'){
                
        $table_user = self::select(['*'])
        ->with(['product' => function($qq) {
        	$qq->select('*');
        }])
        ->with(['stall' => function($q){
            $q->select('*');
        }]
        );
        if($where){
            $table_user->whereRaw($where);
        }
        if(!empty($stall_id)){
            $table_user->where(['stall_id' => $stall_id]);
        }

        //$userlist['userCount'] = !empty($table_user->count())?$table_user->count():0;
        
        if(!empty($order)){
            $order = explode('-', $order);
            $table_user->orderBy($order[0],$order[1]);
        }
        if($fetch === 'array'){
            $userlist = $table_user->get();
            return json_decode(json_encode($userlist ), true );
        }else if($fetch === 'single'){
            return $table_user->get()->first();
        }else{
            return $table_user->get()->first();
        }
    }






    /*public function list_using_joins($fetch='array',$stall_id=NULL,$where='',$order='id-desc') {
        $table_user = DB::table('product_to_stalls as ps');
        $table_user->join('stalls as s', 'ps.stall_id', '=', 's.id');
        $table_user->join('products as p', 'ps.product_id', '=', 'p.id');
        if($where){
            $table_user->whereRaw($where);
        }
        if(!empty($stall_id)){
            $table_user->where(['stall_id' => $stall_id]);
        }

        //$userlist['userCount'] = !empty($table_user->count())?$table_user->count():0;
        $table_user->select('s.name as stall', 's.phone', 's.alternate_phone', 's.email', 'p.name as product', 'p.description', 'p.image', 'p.price', 'p.discount_price', 'p.quantity');
        if(!empty($order)){
            $order = explode('-', $order);
            $table_user->orderBy('ps.'.$order[0],$order[1]);
        }
        if($fetch === 'array'){
            $userlist = $table_user->get();
            return json_decode(json_encode($userlist ), true );
        }else if($fetch === 'single'){
            return $table_user->get()->first();
        }else{
            return $table_user->get()->first();
        }
    }*/

     
}
