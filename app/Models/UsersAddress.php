<?php

namespace Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UsersAddress extends Model
{
	protected $table = 'users_address';
    protected $primaryKey = 'id';
    
    /*const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';*/

    protected $fillable = [
        'user_id',
        'country',
        'state',
        'city',
        'street',
        'latitude',
        'longitude',
        'address_type',
        'status',
        'created_at',     
        'updated_at',
        'pin_code'
    ];

    public function user_address(){
        return $this->hasOne('\Models\Users','id','user_id');
    }

    public static function add($data){
        if(!empty($data)){
            return self::insertGetId($data);
        }else{
            return false;
        }   
    }



    public static function change($userID,$data){
        $isUpdated = false;
        $table_users = DB::table('users_address');
        if(!empty($data)){
            $table_users->where('user_id','=',$userID);
            $isUpdated = $table_users->update($data); 
        }
                
        return (bool)$isUpdated;
    }
}
