<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{

	protected $table = 'categories';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'name',
        'status',
        'created_at',     
        'updated_at'
    ];

   
    public function subcategories() {
        return $this->hasMany('\Models\Subcategory', 'category_id', 'id');
    }

    public function product() {
        return $this->hasMany('\Models\Product', 'category_id', 'id');
    }

    public static function add($data){
        if(!empty($data)){
            return self::insertGetId($data);
        }else{
            return false;
        }   
    }  

    public static function list($fetch='array',$user_id=NULL,$where='',$order='id-desc'){
               
        $table_user = self::select(['*']); //->where('status','=','active');
        if($where){
            $table_user->whereRaw($where);
        }
        if(!empty($user_id)){
            $table_user->where(['id' => $user_id]);
        }

        //$userlist['userCount'] = !empty($table_user->count())?$table_user->count():0;
        
        if(!empty($order)){
            $order = explode('-', $order);
            $table_user->orderBy($order[0],$order[1]);
        }
        if($fetch === 'array'){
            $userlist = $table_user->get();
            return json_decode(json_encode($userlist ), true );
        }else if($fetch === 'obj'){
            return $table_user->limit($limit)->get();                
        }else if($fetch === 'single'){
            return $table_user->get()->first();
        }else{
            return $table_user->limit($limit)->get();
        }
    }


    public static function change($userID,$data){
        $isUpdated = false;
        $table_users = DB::table('categories');
        if(!empty($data)){
            $table_users->where('id','=',$userID);
            $isUpdated = $table_users->update($data); 
        }
                
        return (bool)$isUpdated;
    }
}
