<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Subcategory extends Model
{
	protected $table = 'subcategories';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'status',
        'created_at',     
        'updated_at'
    ];

    public function category() {
    	return $this->hasOne('\Models\Category','id','category_id');
    }

    public function product() {
        return $this->hasMany('\Models\Product', 'subcategory_id', 'id');
    }

    public static function add($data){
        if(!empty($data)){
            return self::insertGetId($data);
        }else{
            return false;
        }   
    }  

    public static function list($fetch='array',$user_id=NULL,$where='',$order='id-desc'){
               
        $table_user = self::select(['*']);  //->where('status','=','active');
        
        if($where){
            $table_user->whereRaw($where);
        }
        if(!empty($user_id)){
            $table_user->where(['id' => $user_id]);
        }

        //$userlist['userCount'] = !empty($table_user->count())?$table_user->count():0;
        
        if(!empty($order)){
            $order = explode('-', $order);
            $table_user->orderBy($order[0],$order[1]);
        }
        if($fetch === 'array'){
            $userlist = $table_user->get();
            return json_decode(json_encode($userlist ), true );
        }else if($fetch === 'obj'){
            return $table_user->limit($limit)->get();                
        }else if($fetch === 'single'){
            return $table_user->get()->first();
        }else{
            return $table_user->limit($limit)->get();
        }
    }


    public static function change($userID,$data){
        $isUpdated = false;
        $table_users = DB::table('subcategories');
        if(!empty($data)){
            $table_users->where('id','=',$userID);
            $isUpdated = $table_users->update($data); 
        }
                
        return (bool)$isUpdated;
    }

    public static function getSubcategory($CategoryId)
    {
       $isUpdated = false;
       $table_user = self::select(['*'])->where('status','=','active');
       
        if(!empty($CategoryId)){
          $isUpdated =  $table_user->where(['category_id' => $CategoryId]);
        }
        return (bool)$isUpdated; 
    }
}
